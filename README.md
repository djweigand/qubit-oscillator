# WARNING
This repository contains a lot of broken code.

Furthermore, some settings use very large amounts of memory, leading to crashes (especially on linux)

# qubit-oscillator

This is a suite to simulate the protocol for encoding qubits in oscillators
proposed in arxiv/1506.05033

The paper that most of this work is based on can be found here:
<http://arxiv.org/pdf/1506.05033v3.pdf>
Of special interest are Figs. 1,2,5,10 and 15. Sec 1C introduces the shift
basis used for the analysis of code states. A brief explanation of the
algorithms used for encoding can be found in sections 2b,c. Section 2e gives
details on the analysis of states, including the analytic result for the
wave-function in v-space, which is used in feedback.py. A brief derivation of
arpe can be found in appendix C

In total, the code has 3400 lines...

## Documentation
The documentation can be found in the gilab wiki (preferred), or in the folder "documentation"

## Workflow

To configure a run, we need to change two files: qubit_oscillator.cfg and wrapper.py

### qubit_oscillator.cfg

This file is parsed by settings.py, defaults and documentation are found there.
Settings are grouped, see example file and code of constants.py for more details.
Notable settings:

- N: Size of the simulated Hilbert space
- Type of and settings for parallelization
- Folder names for stored data and figures
- Debug flags
    - probably __BROKEN__
- Default parameters for the Hamiltonian
    - error rate, coupling strength etc.
- Measured operator
    - GKP Stabilizer, GKP Pauli, Sensor Stabilizer
    - Multiple are allowed, this decides only which operators are stored as global (see  comments on constants.py)

>__!__ The parameter ``FEEDBACK_CHOICES``, which controls the number of samples for imperfect phasegates, is hard-coded into settings.py

### wrapper.py

In most cases, we want to do many parameter sets in one go. This module enables that.
It also acts as interface for kerr.py, which contains the actual code. Basically, this module stores what will be simulated and passes that information on to kerr.py.