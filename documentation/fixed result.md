# High level Simulation (.simulation.fixed_result.py)

## final_state()

defined in simulation.fixed_result.py

The core function for most simulations is .simulation.fixed_result.final_state(encoding, exp_info, *).
This function takes an `Encoding` object and an `Exp_Info` object, and returns one of the following:

- list, containing for each round:
    - Variance in p, Variance in q, weight: Probability to get these results
    - state, Mean shift (In the eigenbasis of the measured  operator, after measurement)
    - state
- final state only:
    - state, weight, correction
    - state, weight

> Each call to final_state corresponds to a single set of system parameters and measurement record, with a single output state.

```python
def final_state(encoding, exp_info,
                state0=None, delta=None, track_frame=False, track_rates=False,
                fit_delta=False, sharpness=False, iterative=False, measured_op_type='S',
                progress_bar=False, M=None, track_states=False, return_ev=False,
                weighing=True):
    """
    Return the state after a series of measurements with result-string n_m
    """
    ...
```

- encoding: ``.feedback.Encoding object``
    - Can be an iterable of two encoding objects. In that case, the measurements alternate. This is useful if we start from vacuum and measure both S_p and S_q.
- exp_info: ``.shared.Exp_Info object``
- state_0: ``qutip.Qobj``
    - initial state, optional
- delta: ``float``
    - if ``state_0`` is not given, use a squeezed vacuum state with squeezing parameter ``delta`` as initial state
    - if neither is given, use the global default ``DELTA`` defined in settings.py
- track_frame:
    - Do not correct the final state, but simply take the shift into account. I.e. the final state is in a shift frame, analogous to a Pauli frame in standard QEC.
    - __???__ Not quite sure about usage
- track_rates:
    - Flag to set the type of output
    - __???__ Usage unknown
- fit_delta:
    - __???__ I think this is used to fit an effective squeezing parameter?
    - probably outdated garbage (If the above is correct)
    - __!__ Supposedly unused
- sharpness:
    - __???__
    - __!__ Supposedly unused
- iterative:
    - This flag is used to imitate and actual experiment:
        1. Start with an empty record
        2. The measurements are simulated by randomly chosing the qubit state based on the reduced density matrix of the qubit
        3. The result is appended to the record, and feedback then generated for the next round
        - This method is useful for a MC approach, where many experiments are simulated
    - The default mode is that the full record is set in advance. This is faster, and useful for brute force simulation of all possible events (only $10^M$, with $M < 12$)
- measured_op_type: ``str``, "S" or "M"
    - Sets whether a GKP or sensor state is generated. This is necessary to use the correct operators in order to compute the effective squeezing parameters.
    - __! IMPROVEMENT__ This can be inferred from exp_info. Possible source for bugs / UserError
- progress_bar:
    - Print current round and alternation. HACK, but useful for some debugging.
- M: ``int``
    - maximal number of rounds. Only used for ``iterative=True``
    - __!!!__ Bad implementation. Only used for ``iterative=True``, otherwise OVERWRITTEN. Add asserts to check for UserError.
- track_states:
    - Flag for type of output
    - __???__ I think this returns also the intermediate states, not only the final one.
- return_ev:
    - Flag for type of output
    - __???__ I think this returns also the mean shift
- weighing:
    - Usually, the probability to obtain this specific measurement record is tracked. This sets the weight to 1.
    - __! Improvement:__ Currently, this changes the weights, but not what the function returns.

### _protocol()

the function final_state obtains the initial state, sets up the simulation and processes the state in between rounds and after the last round. The measurement rounds are handled by a loop, with calls to the funcion ``fixed_result._protocol()``

```python
def _protocol(state, encoding, exp_info,
                return_outcome, alternating=False, iterative=False, progress_bar=False):
    """
    Do the a single round of the protocol in arxiv/1506.05033, Fig 5
    """
```

- state: ``qutip.Qobj``, initial state of the round
- encoding ``Encoding`` object
- encoding ``Exp_Info`` object
- return_outcome: ``bool`` wether the weight should be returned or not. BAD NAME
- alternating: ``bool``
    - The implemented gate is not controlled-S, but controlled \pm S/2. This is more efficient for the photon number and does usually not matter, but alternating measurements are not commuting in this case (Additional Pauli X/Z). This is corrected with an additional displacement
    - __! Improvement__ It might be possible to speed this up or take it as additional frame
- iterative: ``bool`` In case of iterative simulation, the measurement record and the record of measurement errors need to be updated
- progress_bar: ``bool`` Print state and time at certain steps. Useful for debugging

#### How it works

1. Hadamard on ancilla qubit
2. controlled displacement
    - uses a circuit model (matrix multiplication), qutip.mesolve or qutip.mcsolve, depending on exp_info
3. calculate feedback phase (function _feedback and module feedback)
4. Apply the feedback phase to the qubit
5. Hadamard on ancilla qubit
6. Compute probability of measurement outcomes, using qutip.expect().
    - If iterative, also simulate measurement by random choice according to the distr.
    - if x_m is specified, take measurement errors into account
7. Update weight according to result
8. Reset qubit (function shared.qubit_reset)

## _feedback()

Simple switch. The following cases exist

- "state_estimate": feedback computed immediately, using only the state itself ()
    - The current best estimate for the phase can be computed with qutip.expect. This can be used immediately to maximize entropy of the next measurement
    - Fast
- "cat_breeding" variants:
    - As the phases used in the breeding protocol are not actually feedback phases, but the result of the map between breeding and pe, these are in their own module
- everything else is handled by the feedback module, using only the information stored in the ``Encoding`` object
