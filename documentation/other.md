
## Memoization

The state of the system is generally defined recursively. That is, the current state depends on the past measurement record.

In particular, this means that some function calls will be made a lot of times with identical input. Thus, for an expensive function, we want to store the result, for a given input (possibly with constraints)

For example, there are only two possible results for the first measurement. Thus, half of the runs computes feedback for result 0, the other half for result 1, with all other parameters identical.

## Comments on individual files and modules

### settings.py

Global settings. Can be easily configured using the file qubit_oscillator.cfg.
This construction allows to store compact settings files for different simulations.

### constants.py

contains "from qubit_oscillator.settings import *"

The Hilbert space is large, therefore operators take a long time to be constructed.
In this module, most of the used operators are precomputed and stored as global variable.
Tweaking this module and settings.py yields significant differences in startup time.

### feedback.py

Compute feedback phase and current estimate given a measurement record. Uses a lot of memoization to save computing time.
Example: roughly half of the runs have result 0 in the first measurement, it would be bad to compute the feedback every time.

Most of the feedback methods implemented there should not be used.

### shared.py

This module contains a lot of garbage