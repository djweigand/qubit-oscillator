# Simulation details (simulation.shared.py)

## class Exp_Info: Defined in this file

See above for documentation

## product()

```python
def product(oper, state, debug=False):
    """Apply the oper to the state, taking into account whether we have a ket or matrix rep"""
    ...
```

The state we handle might be represented either as ket or as density matrix.
This function handles multiplication with an operator for both cases

Helper to make code more readable and preserve ket states for significant speedup

__! Important__ parameter ``debug`` is unused, possibly there for compatibility?

## displace()

Return a displacement operator in the combined qubit-oscillator system.

__! Important__ SLOW

## phasegate()

```python
def phasegate(state, phase, exp_info):
    """Apply a gate that rotates the qubit by phase, possibly with noisy rotation"""
    ...
```

- In the noiseless case, simply use qutip.phasegate and qutip.tensor to construct the operator. Then apply it to the state
- In the noisy case, it is a bit more complicated:
    1. Compute standard deviation ``sigma = exp_info.error_rate * 2 * np.pi``
    2. Get ``FEEDBACK_CHOICES (int, settings.py)`` random samples from a Gaussian with standard deviation ``sigma`` and their weights ``w``
    3. Transform the initial state to a density matrix if necessary and do
    ```python
    for s in samples:
        state_out += w[s] * phasegate(state_in, s + phase)
    ```

## dict sim_CONTROLLED_FUNC_SWITCH

How the controlled displacement is simulated, depending on exp_info.method

```python
sim_CONTROLLED_FUNC_SWITCH = {'circuit': circuit_controlled,
                           'me': evol_controlled,
                           'mc': mc_controlled}
```

## circuit_controlled()

```python
def circuit_controlled(state, exp_info):
    return product(STAB_CONTR[exp_info.measured_op], state)
```

Do the controlled displacement as ideal gate, using matrix multiplication. The operator is pre-computed in constants.py in order to save computing time

## evol_controlled(), mc_controlled()

```python
def evol_controlled(state, exp_info):
    ...
```

Use either qutip.mesolve or qutip.mcsolve to implement the controlled displacement, with error specification given in ``exp_info``. The sequence is

1. controlled rotation of the oscillator
2. Pauli X on qubit and displacement on oscillator
3. controlled rotation of the oscillator

The procedure is explained in <https://arxiv.org/abs/1506.05033,> the idea is from the Yale group.

__! Improvement__ The two functions have a very large overlap in code.

## qubit_reset()

```python
def qubit_reset(state, x, exp_info):
    """
    Project the qubit to state x and then reset it to 0

    Acts on the joint qubit-cavity state 'state'.

    Args:
        state: Qutip state object, joint cavity-qubit state.
        x: Integer 0 or 1, measurement result

    Returns:
        Qutip state
    """
    ...
```

- Constructing states is expensive, so we do projection->Pauli instead.
- Errors in the projection and idling time are handled here
- Projection is instantaneous, followed by idling time for readout.
- __! Improvement__ Uses only qutip.mesolve. mcsolve might be better in some cases

## remove_qubit()

```python
def remove_qubit(state):
    """
    Remove the qubit from the joint system. Assumes qubit_reset() has been run

    Args:
        state: Qutip state object, joint cavity-qubit state

    Returns:
        Qutip state
    """
    ...
```

- qutip.ptrace always returns a density matrix. If the state is a ket, we can simply throw away some entries instead.
- Gives significant speedup and memory savings
- __HACK !__