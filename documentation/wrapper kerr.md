# qubit_oscillator.cfg

This file is parsed by settings.py, defaults and documentation are found there.
Settings are grouped, see example file and code of constants.py for more details.
Notable settings:

- N: Size of the simulated Hilbert space
- Type of and settings for parallelization
- Folder names for stored data and figures
- Debug flags
    - probably __BROKEN__
- Default parameters for the Hamiltonian
    - error rate, coupling strength etc.
- Measured operator
    - GKP Stabilizer, GKP Pauli, Sensor Stabilizer
    - Multiple are allowed, this decides only which operators are stored as global (see  comments on constants.py)

>__!__ The parameter ``FEEDBACK_CHOICES``, which controls the number of samples for imperfect phasegates, is hard-coded into settings.py

# wrapper.py

In most cases, we want to do many parameter sets in one go. This module enables that.
It also acts as interface for kerr.py, which contains the actual code. Basically, this module stores what will be simulated and passes that information on to kerr.py.

# kerr.py
Usually not called directly. Takes system parameters from wrapper.py, performs the simulation by iterating over all rates, feedback modes and measurement results.

Afterwards, it stores the data, and possibly generates plots.

## parallel_helper()
```python
def parallel_helper(result, rate=None, mode=None, M=None, error_type=None,
                    alternating=None, iterative=None, delta=None, method=None,
                    measured_op_type=None):
    """
    Construct Encoding and Exp_Info classes from arguments and run the simulation

    return eff. squeezing in p/q quadrature and the weight
    """
    ...
    p_z, p_x, weights = sim_fr.final_state(encoding, exp_info, sharpness=True,
                                           track_rates=True, iterative=iterative, delta=delta, M=M,
                                           progress_bar=False, measured_op_type=measured_op_type
                                           ,weighing=False)
    return p_z, p_x, weights
```
Durrently set up as a gigantic switch (8 cases)

## plotting helpers
>Used for old matplotlib version. Behaviour in current ver unknown
### ticks_log_format()
Fixes loglog tick labels

### ticks_format()
Force matplotlib to render numbers with latex

## kerr.py: ``if __name__ == "__main__"``

### iterative_key()
This function has a __BAD NAME__
```python
    def iterative_key(mode, N, delta, rate, args):
        """return the data key, set iterative flag, OVERRIDE args"""
        ...
```
>Contains some hacks, in particular, it overrides the nr of results for some feedback modes (``cat_breeding`` and ``off``)