if __name__ == '__main__':
    import matplotlib
    matplotlib.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.ticker as ticker

#from qubit_oscillator.settings import (N, KAPPA, DATA_PATH, PLOT_PATH, PARALLEL_PROGRESS_BAR)
from qubit_oscillator.constants import (N, KAPPA, DATA_PATH, PLOT_PATH, PARALLEL_PROGRESS_BAR)

import numpy as np
import argparse
from qutip import (destroy, tensor, qeye, create, sigmaz)
import ast          # Boolean arguments
import warnings

import qubit_oscillator.feedback as fb
import qubit_oscillator.simulation.fixed_result as sim_fr
import qubit_oscillator.simulation.shared as shared

import qubit_oscillator.misc as misc
from qubit_oscillator.misc import print_


_DESTROY_C = tensor(destroy(N), qeye(2))
_DESTROY_Q = tensor(qeye(N), destroy(2))
_KERR_C = tensor(create(N)**2 * destroy(N)**2, qeye(2))
_KERR_CQ = tensor(create(N)**2 * destroy(N)**2, sigmaz())


def parallel_helper(result, rate=None, mode=None, M=None, error_type=None,
                    alternating=None, iterative=None, delta=None, method=None,
                    measured_op_type=None):
    """
    Construct Encoding and Exp_Info classes from arguments and run the simulation

    return eff. squeezing in p/q quadrature and the weight
    """
    encoding_param = {'n_m': [],
                      'mode': mode,
                      'error_rate': None}
    exp_info_param = {'H_error_c': None,
                      'noise_ops': None,
                      'error_rate': None,
                      'mixed_projection': False,
                      'imperfect_projection': False,
                      'imperfect_rotation': False,
                      'method': method,
                      'delta': delta}

    if error_type == 'kerr_sz':
        exp_info_param['H_error_c'] = -rate * KAPPA * 0.5 * _KERR_CQ

    elif error_type == 'kerr_qeye':
        exp_info_param['H_error_c'] = -rate * KAPPA * 0.5 * _KERR_C

    elif error_type == 'photon_loss':
        exp_info_param['noise_ops'] = [np.sqrt(rate * KAPPA) * _DESTROY_C]

    elif error_type == 'measurement_errors':
        encoding_param['error_rate'] = rate
        exp_info_param['error_rate'] = rate
        exp_info_param['mixed_projection'] = True

    elif error_type == 'imperfect_projection':
        encoding_param['error_rate'] = rate
        exp_info_param['error_rate'] = rate
        exp_info_param['imperfect_projection'] = True

    elif error_type in ('noiseless'):
        encoding_param['error_rate'] = rate
        exp_info_param['error_rate'] = rate

    elif error_type == 'imperfect_rotation':
        encoding_param['error_rate'] = rate
        exp_info_param['error_rate'] = rate
        exp_info_param['imperfect_rotation'] = True

    elif error_type == 'amplitude_damping':
        exp_info_param['noise_ops'] = [np.sqrt(rate * KAPPA) * _DESTROY_Q]

    if measured_op_type == 'S':
        measured_ops = ['S_p', 'S_q']
    elif measured_op_type == 'M':
        measured_ops = ['M_p', 'M_q']

    if iterative:
        if alternating:
            encoding = [fb.Encoding(**encoding_param),
                        fb.Encoding(**encoding_param)]
        else:
            encoding = fb.Encoding(**encoding_param)
    else:
        assert (not alternating), 'Alternating squares the number of results, are you sure this is correct?'
        encoding_param['n_m'] = result
        encoding = fb.Encoding(**encoding_param)

    if alternating:
        exp_info = [shared.Exp_Info(measured_op=measured_ops[0], **exp_info_param),
                    shared.Exp_Info(measured_op=measured_ops[1], **exp_info_param)]
    else:
        exp_info = shared.Exp_Info(measured_op=measured_ops[0], **exp_info_param)
    p_z, p_x, weights = sim_fr.final_state(encoding, exp_info, sharpness=True,
                                           track_rates=True, iterative=iterative, delta=delta, M=M,
                                           progress_bar=False, measured_op_type=measured_op_type
                                           ,weighing=False)
    return p_z, p_x, weights


# http://stackoverflow.com/questions/19239297/matplotlib-bad-ticks-labels-for-loglog-twin-axis
def ticks_log_format(value, index):
    # For old matplotlib version
    if value == 0:
        return' $0$'
    pwr = np.floor(np.log10(value))
    base = value / (10 ** pwr)
    if pwr == 0 or pwr == 1:
        return '${0:d}$'.format(int(value))
    if -3 <= pwr < 0:
        return '${0:.3g}$'.format(value)
    if 0 < pwr <= 3:
        return '${0:d}$'.format(int(value))
    else:
        return '${0:d}\\times10^{{{1:d}}}$'.format(int(base), int(pwr))


def ticks_format(value, index):
    return '${0:.2g}$'.format(value)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-rate_list", help="Error rates in kHz/2pi used for the simulation",
                        default=[0.00, 0.01, 0.05, 0.1, 0.2], nargs="*", type=float)
    parser.add_argument("-mode_list", help="Encoding modes used for the simulation",
                        default=['arpe'], nargs="*")
    parser.add_argument("-error_type", help="Kerr or photon loss?",
                        default='kerr_sz',
                        choices=['kerr_sz', 'kerr_qeye', 'photon_loss',
                                 'measurement_errors', 'assumed_measurement_errors',
                                 'imperfect_projection', 'noiseless',
                                 'amplitude_damping', 'cat_breeding', 'imperfect_rotation'])
    parser.add_argument("-measured_op_type", help="Encode Operators S or M?",
                        default='S', choices=['S', 'M'])
    parser.add_argument("-nr_results",
                        help="nr of trajectories to simulate iteratively."
                            + "0 for complete (non-iterative) simulation",
                        default=0, type=int)
    parser.add_argument("-alternating",
                        help="Alternating measurements of S_p, S_q. Only S_p if False",
                        default=False, choices=[True, False], type=ast.literal_eval)
    parser.add_argument("-delta", help="DELTA for the initial state", type=float,
                        default=None)
    parser.add_argument("-M", help="Nr of Rounds",
                        default=8, type=int)
    parser.add_argument("-method", help="Simulation method used",
                        default='weighed evolution',
                        choices=['circuit', 'me', 'mc'])
    parser.add_argument("-plot_mode", help="Mode to be plotted",
                        default=['arpe'], nargs='*')
    parser.add_argument("-scale_sigma", help="Scaling for sigma, olny for plotting",
                        default='1/sqrt_pi', choices=['1', '1/sqrt_pi', 'sqrt_2'])
    parser.add_argument("-figname", help="Name for plotted figure, defaults to full description",
                        default=None)
    parser.add_argument("-overwrite", help="Overwrite stored data",
                        default=False, choices=[True, False], type=ast.literal_eval)
    parser.add_argument("-extend", help="Add additional rounds to iterative runs",
                        default=False, choices=[True, False], type=ast.literal_eval)
    parser.add_argument("-plot", help="Plot after evalutation",
                        default=True, choices=[True, False], type=ast.literal_eval)
    parser.add_argument("-print_keys", help="Print stored keys",
                        default=True, choices=[True, False], type=ast.literal_eval)
    parser.add_argument("-heralding_cut", help="Cutoff for heralding",                                          # ???
                        default=1, type=float)
    parser.add_argument("-delete", help="DELETE stored key",
                        default=False, choices=[True, False], type=ast.literal_eval)
    parser.add_argument("-data_file", help="Filename that stores the data",
                        default='kerr')
    parser.add_argument("-plot_flags", help="",
                        default='cat_breeding')
    args = parser.parse_args()

    # When passing numbers as argument, there is a floating point error
    if args.delta is not None:
        delta = round(args.delta, 7)
    else:
        delta = None
    rate_list = [round(arg, 12) for arg in args.rate_list]


    def iterative_key(mode, N, delta, rate, args):
        """return the data key, set iterative flag, OVERRIDE args"""
        if args.plot_flags == 'cat_breeding':

            # Override the nr of results for certain feedback modes
            if mode in ['cat_breeding_B', 'cat_breeding_C', 'cat_breeding_BS', 'fully_random']:
                args.nr_results = 5000
            elif mode == 'off':
                # It does not make sense to do more than 1 run without feedback.
                # HACK. Would be better as assert statement
                args.nr_results = 1

        # Set the iterative flag.
        iterative = not any(mode in args.mode_list
                for mode in ['cat_breeding_B', 'cat_breeding_C', 'cat_breeding_BS', 'fully_random'])
        iterative = iterative and not (args.plot_flags == 'cat_breeding'  and mode =='off')
        iterative = iterative and args.nr_results != 0
        key = (args.measured_op_type, N, args.M, delta, mode, rate, args.error_type, args.alternating, iterative)
        #key = (N, args.M, delta, mode, rate, args.error_type, args.alternating, iterative)
        return iterative, key, args

    if args.extend:
        assert args.nr_results > 0

    print_(args)
    scale_M = 1

    scale_sigma_dict = {'1': 1,
                        '1/sqrt_pi': 1 / np.sqrt(np.pi),
                        'sqrt_2': np.sqrt(2)}
    scale_sigma = scale_sigma_dict[args.scale_sigma]

    print(rate_list)

    # The data is stored in a dict. The keys are tuples and set by iterative_keys()
    # Load stored data from filename args.data_file, if print_keys, print all keys and key_legend
    key_legend = 'N, M, delta, mode, rate, error_type, alternating, iterative'
    data, keys = misc.load(DATA_PATH + args.data_file, args.print_keys, key_legend)

    #keys = []                  # Not sure why this is here

    # args.delete allows to delete specific keys from the data file
    if args.delete:
        print("DELETING the following stored data:")
        print("Legend: " + key_legend)
        for rate in rate_list:
            for mode in args.mode_list:
                iterative, key, args = iterative_key(mode, N, delta, rate, args)
                print(key)
        if input("Proceed (Y/n)?") != "Y":
            print("Aborting")
            exit()
        else:
            print("Proceeding with removal")

    nothing_done = True                 # If nothing has changed, the stored data is not changed
    for rate in rate_list:
        for mode in args.mode_list:
            iterative, key, args = iterative_key(mode, N, delta, rate, args)
            if args.delete:
                data.pop(key, None)
                nothing_done = False
            elif (key not in keys) or args.overwrite or args.extend:
                nothing_done = False
                if key not in keys:
                    print_('args not stored, Simulating', key)
                elif args.overwrite:
                    print_('args stored, but overwrite flag set. Simulating', rate, mode)
                elif args.extend:
                    print_('args stored, but extension flag set. Simulating', rate, mode)
                if iterative:
                    result_list = np.arange(args.nr_results)
                elif (mode in ['cat_breeding_B', 'cat_breeding_C', 'cat_breeding_BS']) or (args.plot_flags == 'cat_breeding'  and mode in ('off', 'fully_random')):
                    result_list = np.zeros((args.nr_results, args.M), dtype=bool)
                else:
                    result_list = misc.result_range(0, 2**args.M, args.M)
                # result is tuple (p_z, p_x, w)
                result = list(zip(
                            *misc.parallel_map(parallel_helper, result_list,
                                               task_kwargs={'rate': rate,
                                                     'mode': mode,
                                                     'M': args.M,
                                                     'delta': delta,
                                                     'method': args.method,
                                                     'alternating': args.alternating,
                                                     'iterative': iterative,
                                                     'error_type': args.error_type,
                                                     'measured_op_type': args.measured_op_type},
                                                     progress_bar=PARALLEL_PROGRESS_BAR
                                               )
                                ))
                result = [np.asarray(r) for r in result]
                if args.extend and (key in keys):
                    result_old = data[key]
                    result_old = [np.asarray(r) for r in result_old]
                    result = [np.concatenate((result_old[i], result[i]))
                                                for i in range(len(result))]
                data[key] = result
                print_('rate, mode done', rate, mode)

    if not nothing_done:
        print_("All done")
        misc.backup(DATA_PATH + args.data_file)
        misc.store(DATA_PATH + args.data_file, data)

    if args.delete:
        print_("Remaining keys")
        data, keys = misc.load(DATA_PATH + args.data_file, True, key_legend)
    if not args.plot or args.delete:
        print_('Plotting turned off or running in delete mode, leaving')
        exit()
    else:
        print_('Plotting')

    figname = args.figname
    fig = plt.figure()
    ax = fig.add_subplot(111)
    fig_title = {'kerr_sz': r'Non-linear dispersive shift',
                 'kerr_qeye': r'Kerr',
                 'photon_loss': r'Photon loss',
                 'amplitude_damping': r'Qubit amplitude damping',
                 'measurement_errors': r'Readout errors',
                 'imperfect_projection': r'Imperfect projection',
                 'assumed_measurement_errors': r'Noiseless, method AVERAGE',
                 'noiseless': r'Ideal case',
                 'cat_breeding': r'',
                 'imperfect_rotation': r'Noisy phasegate'}
    legend_title = {'kerr_sz': r'$K_{{cq}} /2\pi,\ \mathrm{{kHz}}$',
                 'kerr_qeye': r'$K_c /2\pi,\ \mathrm{{kHz}}$',
                 'photon_loss': r'$\kappa /2\pi,\ \mathrm{{kHz}}$',
                 'amplitude_damping': r'$\gamma /2\pi,\ \mathrm{{kHz}}$',
                 'measurement_errors': r'$p, \%$',
                 'assumed_measurement_errors': r'$p, \%$',
                 'imperfect_projection': r'$p, \%$',
                 'noiseless': '',
                 'cat_breeding': '',
                 'imperfect_rotation': r'$\sigma/\pi$'}
    legend_scale = {'kerr_sz': 1,
                 'kerr_qeye': 1,
                 'photon_loss': 1,
                 'amplitude_damping': 1,
                 'measurement_errors': 100,
                 'assumed_measurement_errors': 1,
                 'imperfect_projection': 100,
                 'noiseless': 1,
                 'cat_breeding': 1,
                 'imperfect_rotation': 1}
    mode_labels = {'cat_breeding_B': 'Breeding',
                   'cat_breeding_BS': 'BSS',
                   'cat_breeding_C': 'BCNOT',
                   'state_estimate': 'PE',
                   'estimate': 'PE',
                   'arpe': 'ARPE',
                   'rpe': 'RPE',
                   'off': 'Binomial'}

#    plt.rc('font',
#        size=11,
#        **{'family': 'serif', 'serif': ['Computer Modern']})
    plt.rc('font',
        size=16)
    lines = []
    labels = []
    for rate in rate_list:
        for mode in args.plot_mode:
#            if args.error_type in ('cat_breeding', 'imperfect_rotation'):
#                if mode == 'estimate':
#                    iterative = True
#                else:
#                    iterative = False
            iterative, key, args = iterative_key(mode, N, delta, rate, args)
            try:
                p_z, p_x, w = data[key]
            except KeyError:
                print_('The key {} has not yet been simulated. Stored Data is:'.format(key))
                for key in keys:
                    print_(key)
                raise
            if len(p_z.shape) == 3:
                p_z = p_z.reshape((p_z.shape[0], p_z.shape[1]))
            if len(p_x.shape) == 3:
                p_x = p_x.reshape((p_x.shape[0], p_x.shape[1]))
            if len(w.shape) == 3:
                w = w.reshape((w.shape[0], w.shape[1]))
            p_z *= scale_sigma
            p_x *= scale_sigma
            #p_c = (p_z + p_x) / 2
            #_, w, m = misc.heralding(p_c, w, args.heralding_cut, axis=0, return_mask=True)
            #p_z = misc.mask(p_z, m, default=1)
            #p_z = np.average(p_z, weights=w, axis=0)
            mean_z, std_z = misc.weighted_avg_and_std(p_z, weights=w, axis=0)
            mean_x, std_x = misc.weighted_avg_and_std(p_x, weights=w, axis=0)
            #p_z = misc.weighted_quantile(p_z, 95, weights=w, axis=0)
            #p_x = misc.mask(p_x, m, default=1)
            #p_x = np.average(p_x, weights=w, axis=0)
            #p_x = misc.weighted_quantile(p_x, 95, weights=w, axis=0)
            if len(rate_list) > 1 and len(args.plot_mode) == 1:
                label = "${:g}$".format(rate * legend_scale[args.error_type])
            else:
                mode_label = mode_labels.get(mode)
                if mode_label is None:
                    mode_label = mode
                #label = "{}, ${:g}$".format(mode_label, rate * legend_scale[args.error_type])
                label = "{}".format(mode_label)
            labels.append(label)
            idx = np.arange(len(mean_z)) * scale_M
            (line,) = ax.plot(idx, mean_z)
            lines.append(line)
            c = line.get_color()
            if args.plot_flags[:3] == 'aps':
                ax.plot(idx, np.abs(mean_x), color=c, linestyle='--')
            elif args.plot_flags == 'cat_breeding':
                ax.plot(idx, std_z, color=c, linestyle='--')
            else:
                raise Exception()

    x_lbl = ax.set_xlabel('$M$', size=16)
    y_lbl = ax.set_ylabel(r'$\Delta$', rotation=0, size=16)

    ax.tick_params(which='major', length=8, width=1.5)
    ax.tick_params(which='minor', length=4, width=1.5)
    ax.tick_params(which='both', top=False, right=True, labelsize=16,
direction='in')

    ax.xaxis.set_major_formatter(ticker.FuncFormatter(ticks_format))
    ax.set_xticks(range(13), minor=True)
    ax.set_xticks([0, 4, 8, 12], minor=False)
    ax.set_xticklabels([0, 4, 8, 12], minor=False)
    ax.set_xlim(0, args.M)

    if args.plot_flags[:3] == 'aps':
        ax.set_yscale('log')
        ax.set_yticks([0.1, 0.5, 1])
        ax.set_yticklabels(['']*10, minor=True)
        ax.set_yticklabels([0.1, 0.5, 1], minor=False)
        ax.set_ylim(0.1, 1)
        fig_ttl = fig.suptitle(fig_title[args.error_type])
        fig.set_size_inches(1.5*3.405, 3.405)
        if args.plot_flags == 'aps-noiseless':
            solid = matplotlib.lines.Line2D([], [], color=c, linestyle='-',
                              label='$\Delta_{p}$')
            dashed = matplotlib.lines.Line2D([], [], color=c, linestyle='--',
                              label='$\Delta_{q}$')
            lines = (solid, dashed)
            labels = ('$\Delta_{p}$', '$\Delta_{q}$')
        if args.plot_flags == 'aps-noise':
            solid = matplotlib.lines.Line2D([], [], color='black', linestyle='-',
                              label='$\Delta_{p}$')
            dashed = matplotlib.lines.Line2D([], [], color='black', linestyle='--',
                              label='$\Delta_{q}$')
            lines = (solid, dashed) + tuple(lines)
            labels = ('$\Delta_{p}$', '$\Delta_{q}$') + tuple(labels)
        lgd = plt.legend(lines, labels, title=legend_title[args.error_type],
               loc='upper left', bbox_to_anchor=(1.01, 1.05),
               ncol=1)
        plt.savefig(PLOT_PATH + figname + '.png', bbox_extra_artists=(lgd,fig_ttl,), bbox_inches='tight')
    elif args.plot_flags == 'cat_breeding':
        ax.set_ylim(0, 1)
        fig.set_size_inches(3.405, 3.405)
        fig.tight_layout(pad=0)
        solid = matplotlib.lines.Line2D([], [], color='black', linestyle='-',
                          label=r'$\langle \Delta_{p} \rangle$')
        dashed = matplotlib.lines.Line2D([], [], color='black', linestyle='--',
                          label=r'$\sigma(\Delta_{q})$')
        lines.append(solid)
        labels.append(r'$\langle \Delta \rangle$')
        lines.append(dashed)
        labels.append(r'$\sigma(\Delta)$')
        #lgd0 = plt.legend(handles=[solid, dashed], loc='upper right', bbox_to_anchor=(1.01, 0.75),ncol=1)
        lgd = fig.legend(lines, labels, title=legend_title[args.error_type],
               loc='upper right', bbox_to_anchor=(0.98, 1),
               ncol=1)
        fig.savefig(PLOT_PATH + figname + '.png',
                bbox_extra_artists=(lgd))
    else:
        raise Exception()
    plt.close()
