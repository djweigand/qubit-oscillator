"""
Convention: All constants defined here are UPPER CASE
"""

# import sys
# if 'matplotlib' not in sys.modules:
#   raise ImportWarning('Matplotlib has to be imported BEFORE any modules in qubit_oscillator')

from qubit_oscillator.settings import N, CHI, DELTA, MEASURED_OPS
# Make everything that is not used here public
from qubit_oscillator.settings import *
import warnings

import numpy as np
warnings.filterwarnings('ignore', category=np.VisibleDeprecationWarning)
from qutip import (tensor, qeye, num, snot, displace,
                   sigmax, sigmaz, fock_dm, squeeze, basis)


class smart_dict(dict):
    def __init__(self, *args, **kwargs):
        self.function = kwargs.pop('function')
        dict.__init__(self, *args, **kwargs)

    def __missing__(self, key):
        self[key] = self.function(key)
        return self.function(key)


QEYE_N = qeye(N)
NUM_Q = tensor(qeye(N), num(2))
SX = tensor(qeye(N), sigmax())
HADAMARD = tensor(qeye(N), snot())                  # Hadamard Gate

H_IDEAL = -CHI * tensor(num(N), sigmaz())
H_IDLE = tensor(qeye(N), qeye(2))
PROJECT = [tensor(qeye(N), fock_dm(2, 0)), tensor(qeye(N), fock_dm(2, 1))]
PSI0 = tensor(squeeze(N, np.log(1 / DELTA)) * basis(N, 0), basis(2, 0)).unit()
VAC = tensor(basis(N, 0), basis(2, 0)).unit()

# Constants for plotting
# width of a quarter aps page in inches
LATEX_WIDTH = 0.48 * 3.4045

DISPLACEMENT_VALUES = {"S_p": np.sqrt(2 * np.pi),
                       "S_q": 1j * np.sqrt(2 * np.pi),
                       "X": np.sqrt(np.pi / 2),
                       "Z": 1j * np.sqrt(np.pi / 2),
                       "M_p": np.sqrt(np.pi),
                       "M_q": 1j * np.sqrt(np.pi)
                       }


def _disp_values(value):
    assert type(value) in (np.float64, complex)
    return value
DISPLACEMENT_VALUES = smart_dict(DISPLACEMENT_VALUES, function=_disp_values)


def _disp(value):
    return displace(N, value)
STABILIZERS = smart_dict(function=_disp)


def _stab_contr(value):
    return tensor(_disp(value / 2), fock_dm(2, 1)) + tensor(_disp(value / 2).dag(), fock_dm(2, 0))
STAB_CONTR = smart_dict(function=_stab_contr)


def _stab_q(value):
    return tensor(_disp(value), qeye(2))
STABILIZERS_Q = smart_dict(function=_stab_q)


def _stab_rot(value):
    return tensor(displace(N, -1j * value / 2), qeye(2))
STAB_ROT = smart_dict(function=_stab_rot)


def _ev_scale(value):
    return np.sqrt(2) * value
EIGENVALUE_SCALE = smart_dict(function=_ev_scale)

STAB_SQRT = {}

for key, value in DISPLACEMENT_VALUES.items():
    EIGENVALUE_SCALE[key] = np.sqrt(2) * np.abs(value)
    if key in MEASURED_OPS:
        d = displace(N, value)
        d2 = displace(N, value / 2)
        disp_2_q = tensor(d2, qeye(2))
        STAB_CONTR[key] = (tensor(d2, fock_dm(2, 1))
                               + tensor(d2.dag(), fock_dm(2, 0)))
        STAB_SQRT[key] = [disp_2_q, disp_2_q.dag()]
        STAB_ROT[key] = tensor(displace(N, -1j * value / 2), qeye(2))
        STABILIZERS[key] = d
        STABILIZERS_Q[key] = tensor(d, qeye(2))
