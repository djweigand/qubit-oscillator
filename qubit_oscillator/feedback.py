# -*- coding: utf-8 -*-

"""
This module provides functions for several types of feedback

mode 0: Kitaev phase estimation. arxiv: quant-ph/9511026
mode 1: Adaptive repeated phase estimation PRA 63, 053804 (2001)
mode 2: Feedback off, Returns 0.
mode 3: returns a random number depending on m, x

More information can be found in arxiv: 1506.05033 (our paper) or my
M.Sc. thesis

Algorithm for Arpe:
    Following PRA 63, 053804 (2001) (readable summeries in our paper or my
    thesis), we optimize the Holevo variance of P(theta|n_m) where theta is
    the eigenvalue estimate of the measurement given the bit-string
    of results n_m.

    The probability distribution given result n_m is computed in P(m, x, mode)
    The function _S_optimize(phi, m, x, mode) computes the Holevo variance of
    and then averages over P(m+1, x, mode) and P(m+1, x x + 2^(m+1), mode),
    (in this example x < 2^m).
    Then, the optimal feedback is found in feedback_function(m, x, mode=1)
    using scipy.optimize.
    The function feedback(m, x, mode) is a wrapper to save time, as the
    feedback depends only on previous results, but x is the current result
    (thus holds e.g. feedback(1,0,mode)==feedback(1,1,mode) and so on)

    The wave function in u, v basis after the protocol can be obtained
    analytically and is implemented in psiv_noc(v, m, x, mode).
    The optimal estimate for the eigenvalue can be obtained by an integral over
    P(m, x, mode), this is done in estimate(m, x, mode)
    Using this estimate, the center of the wave function is shifted to v=0 in
    psiv(v, m, x, mode).

    With the corrected wave function, the probability to obtain a good codeword
    is obtained in P_no_error(m, x, mode), which integrates over psiv() in the
    interval v \in (-\sqrt{\pi}/6,\sqrt{\pi}/6).

    If the module is called as script, P_no_error() is evaluated for all 0<m<9,
    all x and all modes, the results are shown as a histogram.

Conventions:
    n_m: List of the binary measurement results. Entry m is the result of the
            m-th round.
    m: All functions take a round m as argument. This truncates the bit-string
        n_m, to the first m entries.
        e.g. P(m=1,x=1,mode) = P(m=1, x=7, mode)
    x: Some functions are memoized. For efficient memoizing, a function cannot
        take lists as input. Thus, the list n_m is converted to the integer x
"""
from qubit_oscillator.constants import *

if __name__ == '__main__':
    import matplotlib.pylab as plt

from qutip import *
from scipy.special import binom
from scipy.stats import binom as binomial_dist
import numpy as np
import numpy.ma as ma
import scipy.integrate
import scipy.optimize as opt
import pickle
import time
import sys
import itertools
import warnings
import copy

from qubit_oscillator.misc import Memoize, bin_to_int
import qubit_oscillator.misc as misc

# import test_data

_THETA = np.linspace(0, 2 * np.pi, 2**10 + 1)
_THETA_EXP = np.exp(1j * _THETA)


_VALID_ENCODING_MODES = ('rpe', 'rpe_e',
                        'arpe', 'arpe_e', 'arpe_2',
                        'average', 'average_2',
                        'sum', 'events',
                        'estimate', 'state_estimate', 'equal',
                        'random', 'random_e', 'fully_random',
                        'cat_breeding_B', 'cat_breeding_BS', 'cat_breeding_C',
                        'off', 'list')


class Encoding:
    """
    Store all information needed for the classical processing of phase estimation

    Class is designed such that invalid arguments (e.g. x_m defined but not
    error_rate) will raise an error
    """
    def __init__(self, m=None, n_m=None, mode=None, error_rate=None, phases=None):
        assert mode in _VALID_ENCODING_MODES, 'Mode not defined: {}'.format(mode)
        assert m == 0 or n_m is not None
        if m == 0:
            assert n_m in (None, [])
            self.m = 0
            self.n_m = []
        elif m is None:
            self.m = len(n_m)
            self.n_m = n_m
        else:
            self.m = m
            self.n_m = np.take(n_m, np.arange(m))
        self.mode = mode
        self._error_rate = error_rate
        if error_rate is not None:
            self.error_rate = self._error_rate
        if phases is not None:
            self.phases = phases

    def __hash__(self):
        return hash(pickle.dumps((self.m, self.n_m, self.mode, self._error_rate), protocol=-1))

    def __str__(self):
        return str((self.m, self.n_m, self.mode, self._error_rate))

    def shorten(self):
        self.n_m = np.take(self.n_m, np.arange(self.m))

    def set_m(self, m):
        cp = copy.deepcopy(self)
        cp.m = m
        cp.shorten()
        return cp

    def m_minus_1(self):
        return self.set_m(self.m - 1)

    def update_n(self, n):
        cp = copy.deepcopy(self)
        cp.m += 1
        cp.n_m.append(n)
        return cp

    def remove_e(self):
        if self.mode[-2:] == '_e':
            cp = copy.deepcopy(self)
            cp.mode = self.mode[:-2]
        else:
            cp = self
        return cp


class Memoize_Encoding_scalar(misc.memoize.Memoize):
    def __call__(self, encoding):
        m = encoding.m
        n = bin_to_int(encoding.n_m)
        key = (m, encoding.mode, encoding._error_rate)

        memo_array = self.memo.get(key)
        if memo_array is None:
            memo_array = ma.array(np.empty(2**m), mask=np.ones(2**m))
            result = self.f(encoding)
            memo_array[n] = result
        else:
            if memo_array.mask[n]:
                result = self.f(encoding)
                memo_array[n] = result
            else:
                result = memo_array[n]
        self.memo[key] = memo_array
        return np.copy(result)


class Memoize_Encoding_array(misc.memoize.Memoize):
    def __call__(self, encoding):
        m = encoding.m
        n = bin_to_int(encoding.n_m)
        key = (m, encoding.mode, encoding._error_rate)

        memo_array = self.memo.get(key)
        if memo_array is None:
            memo_array = ma.array(np.empty((2**m, 2**10+1)), mask=np.ones((2**m, 2**10+1)))
            result = self.f(encoding)
            memo_array[n] = result
        else:
            if memo_array.mask[n, 0]:
                result = self.f(encoding)
                memo_array[n] = result
            else:
                result = memo_array[n]
        self.memo[key] = memo_array
        return np.array(result, copy=True)


class Memoize_limited(misc.memoize.Memoize):
    def __call__(self, *args):
        arglist = list(args)
        for i, arg in enumerate(arglist):
            if isinstance(arg, (np.ndarray, list)):
                if len(arg) > 8:
                    return self.f(*args)
                arglist[i] = pickle.dumps(arg, protocol=-1)
        key = tuple(arglist)
        if key not in self.memo:
            self.memo[key] = self.f(*args)
        return self.memo[key]
# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# FUNCTIONS


def P(encoding):
    encoding = encoding.remove_e()
    return _P(encoding)


@Memoize_Encoding_array
def _P(encoding):
    """
    Compute the probability P(theta|x) of initial phase theta given results x.

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Array P(theta|x)
    """

    if encoding.m == 0:
        return 1
    phi = feedback(encoding)
    arg = 0.5*(np.pi*encoding.n_m[encoding.m-1] + phi +_THETA)
    np.cos(arg, out=arg)
    np.power(arg, 2, out=arg)
    p = P(encoding.m_minus_1())
    p = p * arg
    return p


@Memoize_Encoding_array
def P_sum(encoding):
    if encoding.m == 0:
        return 1
    phi = feedback(encoding)
    arg = 0.5*(np.pi*encoding.n_m[encoding.m-1] + phi +_THETA)
    np.cos(arg, out=arg)
    np.power(arg, 2, out=arg)
    p = P_sum(encoding.m_minus_1())
    p = p + arg                              # This is the only difference to P
    return p


def P_av(encoding):
    encoding = encoding.remove_e()
    return _P_av(encoding)


@Memoize_Encoding_array
def _P_av(encoding):
    if encoding.m == 0:
        return 1
    error_rate = encoding.error_rate
    phi = feedback(encoding)
    arg = 0.5*(np.pi*encoding.n_m[encoding.m-1] + phi +_THETA)
    np.cos(arg, out=arg)
    np.power(arg, 2, out=arg)
    p = P_av(encoding.m_minus_1())
    p = p * ((1-2*error_rate) * arg + error_rate)
    return p


@Memoize_limited
def P_E(encoding, x_m):
    # error_rate argument should ALWAYS be specified if using this func
    if encoding.m == 0:
        return 1
    encoding = encoding.remove_e()
    x_m = np.take(x_m, np.arange(encoding.m))
    true_result = encoding.n_m ^ x_m
    p = P_E(encoding.m_minus_1(), x_m)
    p = p * np.cos(0.5*(np.pi*true_result[encoding.m-1] + feedback(encoding)+_THETA))**2
    return p


P_switch = {'arpe': P,
            'arpe_2': P,
            'equal': P,
            'sum': P_sum,
            'average': P_av,
            'average_2': P_av,
            'events': P_E}

# phase estimate of result x at round m
@Memoize_Encoding_scalar
def estimate(encoding):
    """
    Compute the estimate of the protocol given results x.

    Uses the algorithm of Berry, Wiseman, Breslin, PRA 63, 053804 (2001).
    More information can be found in arxiv: 1506.05033 (our paper) or my
    M.Sc. thesis

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Float, best estimate given the results
    """

    # Note that the numpy doc on np.angle is wrong,
    # the angle is in the interval (-pi,pi).
    mode = encoding.mode
    if (mode in ("average", 'average_2', "events")) or (mode[-2:] == '_e'):
        p = P_av(encoding)
    elif mode in ("rpe", "arpe", 'random', 'arpe_2','off', 'estimate'):
        p = P(encoding)
    elif mode == "sum":
        p = P_sum(encoding)
    p = p * _THETA_EXP
    return np.angle(scipy.integrate.romb(p, 2*np.pi/(2**10 + 1)))

@Memoize_Encoding_scalar
def sharpness(encoding):
    """
    Compute the estimate of the protocol given results x.

    Uses the algorithm of Berry, Wiseman, Breslin, PRA 63, 053804 (2001).
    More information can be found in arxiv: 1506.05033 (our paper) or my
    M.Sc. thesis

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Float, best estimate given the results
    """

    # Note that the numpy doc on np.angle is wrong,
    # the angle is in the interval (-pi,pi).
    mode = encoding.mode
    if (mode in ("average", 'average_2', "events")) or (mode[-2:] == '_e'):
        p = P_av(encoding)
    elif mode in ("rpe", "arpe", 'random', 'arpe_2','off', 'estimate'):
        p = P(encoding)
    elif mode == "sum":
        p = P_sum(encoding)
    p = p * _THETA_EXP
    return np.abs(scipy.integrate.romb(p, 2*np.pi/(2**10 + 1)))


def estimate_E(encoding, x_m):
    p = P_E(encoding, x_m)*np.exp(1j*_THETA)
    return np.angle(scipy.integrate.romb(p, 2*np.pi/(2**10 + 1)))


# -----------------------------------------------------------------------------
# FEEDBACK

# Functions needed to obtain the feedback in various protocols are defined here
# An explanation of the protocols is found in the doc of feedback_function

def feedback(encoding, iterative=False):
    """
    wrapper for feedback_function, saves some time (f(1,0) = f(1,1) etc.)

    the feedback does not depend on the last measurement result, as it is
    applied before the last measurement. This wrapper helps with memoization.

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Float, feedback
    """
    # Remove marker '_e', only affects estimate
    encoding = encoding.remove_e()
    if encoding.mode == 'fully_random':
        return 2 * np.pi * np.random.random_sample()
    elif encoding.mode == 'list':
        return encoding.phases[encoding.m - 1]      # numbering in fixed_results starts at 1
    elif iterative:
        return feedback_function(encoding)
    else:
        return feedback_function(encoding.m_minus_1())

@Memoize_Encoding_scalar
def feedback_function(encoding):
    """
    Compute feedback depending on mode (rpe, arpe, off). wrapped by feedback()

    mode 0: Kitaev phase estimation. arxiv: quant-ph/9511026
    mode 1: Adaptive repeated phase estimation PRA 63, 053804 (2001)
    mode 2: Feedback off, Returns 0.
    mode 3: returns a random number depending on m, x

    More information can be found in arxiv: 1506.05033 (our paper) or my
    M.Sc. thesis

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Float, feedback
    """
    mode = encoding.mode
    if mode in ('arpe', 'average', 'events', 'sum', 'equal'):
        if encoding.m == 0:
            # if no measurement has been made, any feedback is ok. Choose 0.
            return 0
        opt_fb = opt.fminbound(_S_optimize, 0, 2*np.pi,
                               args=(encoding,))
        if DEBUG:
            _feedback_function_DEBUG(encoding, opt_fb)
        return opt_fb

    elif mode == 'rpe':
        return np.mod(encoding.m*np.pi/2, np.pi)

    elif mode == 'estimate':
        if encoding.m == 0:
            # if no measurement has been made, any feedback is ok. Choose 0.
            return 0
        return -estimate(encoding) + np.pi/2

    elif mode in ('arpe_2', 'average_2', 'events_2'):
        opt_fb = _S2_storage(encoding)
        if DEBUG:
            _feedback_function_DEBUG(encoding, opt_fb)
        return opt_fb[0]

    elif mode == 'off':
        return 0

    elif mode == 'random':
        # Memoization ensures that multiple calls with same args yield same res
        if encoding.m == 0:
            np.random.seed()
        return 2 * np.pi * np.random.random_sample()

def _feedback_function_DEBUG(encoding, opt_fb):
    assert encoding.m >= 0

    if encoding.mode == 'arpe_2':
        S_ = _S2_optimize
        ranges = ((0, 2*np.pi), (0, 2*np.pi))
    else:
        S_ =  _S_optimize
        ranges = ((0, 2*np.pi),)
    x = np.linspace(0, 2*np.pi, 100).reshape(-1,1)
    y = np.linspace(0, 2*np.pi, 100).reshape(1,-1)

    opt_fb_test = opt.brute(S_, ranges,
                            args=(encoding,), Ns=100)

    if np.any(np.abs(opt_fb - opt_fb_test) > 10**(-2)):
        opt_fb_S = S_(opt_fb, encoding)
        opt_fb_test_S = S_(opt_fb_test, encoding)
        if np.any(np.abs(-opt_fb_S + opt_fb_test_S) > 10**(-2)):
            message = ("Feedback does not match brute force search, "
                +"m {}, n_m {}, mode {}: \n Opt {}, {}; Brute {}, {}".format(
                                    encoding.m, encoding.n_m, encoding.mode,
                                    opt_fb, opt_fb_S,
                                    opt_fb_test, opt_fb_test_S))
            if DEBUG_RAISE:
                raise Exception(message)
            else:
                warnings.warn(message)

def _S_optimize(phi, encoding):
    """
    Form Average of the Holevo variance over results u=0,1

    Helper function for adaptive repeated phase estimation.
    More information on the algorithm can be found in arxiv/1506.05033 (our
    paper), or Berry, Wiseman, Breslin, PRA 63, 053804 (2001).

    Args:
        phi: float, 'true' phase that is measured
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Float, feedback
    """

    mode = encoding.mode
    P_ = P_switch[mode]

    if mode in ('average', 'arpe', 'sum'):
        p = P_(encoding).reshape(1,-1)
        u = np.arange(2).reshape((2, 1))
        if mode == "arpe":
            p = p * np.cos(0.5*(np.pi*u+phi+_THETA.reshape(1,-1)))**2
        elif mode == 'average':
            p = p * ((1-2*encoding.error_rate)*np.cos(0.5*(np.pi*u+phi+_THETA.reshape(1,-1)))**2 + encoding.error_rate)
        elif mode == 'sum':
            p = p + np.cos(0.5*(np.pi*u+phi+_THETA.reshape(1,-1)))**2
        p = p*_THETA_EXP.reshape(1, -1)
        S = scipy.integrate.romb(p, 2*np.pi/(2**10 + 1), axis=1)
        return -np.sum(np.abs(S))

    elif mode == 'equal':
        p = P_(encoding).reshape(1,-1)
        u = np.arange(2).reshape((2, 1))
        p = p * np.cos(0.5*(np.pi*u+phi+_THETA.reshape(1,-1)))**2
        p = scipy.integrate.romb(p, 2*np.pi/(2**10 + 1), axis=1)
        return np.abs(np.sum(np.array([1,-1]) * np.abs(p)))

    elif mode == "events":
        possible_errors = misc.error_range(ERROR_WEIGHT_FEEDBACK, m+1)
        possible_errors = np.asarray(possible_errors)
        k = np.sum(possible_errors, axis=-1)
        weight = (1-encoding.error_rate)**(encoding.m+1-k) * encoding.error_rate**k
        S_=[]
        for u in np.arange(2):
            for error in possible_errors:
                x_m = np.take(error, np.arange(encoding.m))
                p = P_E(encoding, x_m)
                if error[-1]:
                    p *= np.cos(0.5*(np.pi*((u+1)%2)+phi+_THETA))**2
                else:
                    p *= np.cos(0.5*(np.pi*(u)+phi+_THETA))**2
                S_.append(_THETA_EXP*p)
        S_ = np.asarray(S_)
        S = scipy.integrate.romb(S_, 2*np.pi/(2**10 + 1), axis=-1)
        S = np.abs(S)
        S = weight*S
        return -np.sum(S)


@Memoize
def _S2_storage(encoding, guess=0):
    if m == 0:
        guess = np.pi/2
    else:
        _, guess = _S2_storage(encoding.m_minus_1())
    guess = (guess, (guess+np.pi/2)%(2*np.pi) )
    return opt.minimize(_S2_optimize, guess,
                        args=(encoding),
                        method='TNC',
                        bounds=((0, 2*np.pi), (0, 2*np.pi))
                        ).x

def _S2_optimize_vec(phi1, phi2, encoding):
    phi = (phi1, phi2)
    return _S2_optimize(phi, encoding)

def _S2_optimize(phi, encoding):
    phi1, phi2 = phi
    P_ = P_switch[encoding.mode]

    u1 = np.arange(2).reshape(2,1,1)
    u2 = np.arange(2).reshape(1,2,1)
    p=P_(encoding)

    if encoding.mode == "arpe_2":
        p = p * np.cos(0.5*(np.pi*u1+phi1+_THETA.reshape(1,1,-1)))**2
        p = p * np.cos(0.5*(np.pi*u2+phi2+_THETA.reshape(1,1,-1)))**2
    elif encoding.mode == 'average_2':
        p = p * ((1-2*encoding.error_rate)*np.cos(0.5*(np.pi*u1+phi1+_THETA.reshape(1,1,-1)))**2 + encoding.error_rate)
        p = p * ((1-2*encoding.error_rate)*np.cos(0.5*(np.pi*u2+phi2+_THETA.reshape(1,1,-1)))**2 + encoding.error_rate)
    p =  p * _THETA_EXP.reshape(1,1,-1)
    S = scipy.integrate.romb(p, 2*np.pi/(2**10 + 1), axis=-1)
    S_sum = np.sum(np.abs(S))
    return -S_sum


# -----------------------------------------------------------------------------
# STATE


def psiv(v, encoding):
    """
    Return the corrected state after the idealized protocol u, v basis.

    Return the state in u,v basis after applying a correcting shift obtained
    by estimate().
    The function assumes a delta-peak as initial state (u=0). This can be done
    as the errors in u and v commute and u is computed with other means.
    This assumption is tested and holds.

    Args:
        v: Linspace in interval (-np.sqrt(np.pi)/6, np.sqrt(np.pi)/6),
            shift error v.
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Array, Abs**2 of the wavefunction for the values of v
    """

    # The estimate is added, as we measure -v, where v is the shift (see paper)
    vc = v + estimate(encoding)/(2*np.sqrt(np.pi))
    return psiv_noc(vc, encoding)


def psiv_noc(v, encoding):
    """
    Return the uncorrected state after the idealized protocol u, v basis.

    Return the state in u,v basis.
    The function assumes a delta-peak as initial state (u=0). This can be done
    as the errors in u and v commute and u is computed with other means.
    This assumption is tested and holds.

    Args:
        v: Linspace in interval (-np.sqrt(np.pi)/6, np.sqrt(np.pi)/6),
            shift error v.
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Integer, mode for feedback

    Returns:
        Array, Abs**2 of the wavefunction for the values of v
    """
    psi = np.zeros(len(v), dtype=np.complex_)
    for s in np.arange(encoding.m+1):
        psi += np.exp(1j*np.sqrt(np.pi)*2*s*v)*_distrib(encoding, s)
    return psi/_norm(encoding)


# Checked, seems good
# checked second time, in depth. algorithm is OK.
@Memoize
def _distrib(encoding, s):
    """
    Obtain distribution of squeezed states forming a code state defined by x

    More information can be found in arxiv: 1506.05033 (our paper) or my
    M.Sc. thesis

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        s: Integer, running index of the squeezed state
        mode: Mode (rpe, arpe, off) for feedback integer 0,1 or 2

    Returns:
        Complex, Weight of the squeezed state s in a code state
    """
    if s == 0:
        return 1
    m = encoding.m
    n_comb = np.int(binom(m, s))
    phases = np.zeros(m, dtype=np.complex_)
    for i in np.arange(m):
        phases[i] = feedback(encoding.set_m(i+1)) + encoding.n_m[i]*np.pi
    t = itertools.chain.from_iterable(itertools.combinations(phases, s))
    t = np.fromiter(t, dtype=np.complex_, count=n_comb*s)
    t.resize((n_comb, s))
    t = np.exp(1j*t.sum(axis=1))
    return t.sum()


def _norm(encoding):
    """
    Approximate the norm of a code state after m measurements with result x

    The function assumes that the squeezed states are approximately orthogonal.
    If a codeword is even slightly useful, this assumption is good to 10^-9.

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Mode (rpe, arpe, off) for feedback integer 0,1 or 2

    Returns:
        Float, norm of the state
    """
    nrm = 0
    for s in np.arange(encoding.m+1):
        nrm += np.abs(_distrib(encoding, s))**2
    return np.sqrt(nrm*np.sqrt(np.pi))

# -----------------------------------------------------------------------------
# ANALYSIS


#@Memoize_Encoding_scalar
def P_no_error(encoding):
    """
    Compute the probability to obtain a good code state given m, x and mode

    Args:
        m: Integer, current round of the protocol
        x: Integer, measurement results of the whole protocol
        mode: Mode (rpe, arpe, off) for feedback integer 0,1 or 2

    Returns:
        Float, probability
    """
    v = np.linspace(-np.sqrt(np.pi)/6, np.sqrt(np.pi)/6, 2**10 + 1)
    psi = np.abs(psiv(v, encoding))**2
    return scipy.integrate.romb(psi, np.sqrt(np.pi)/(3*(2**10 + 1)))


def reset(*args):
    _P.reset()
    _P_av.reset()
    P_E.reset()
    feedback_function.reset()
    _distrib.reset()
    #P_no_error.reset()
    time.sleep(1)

# -----------------------------------------------------------------------------
# MISC

def _parallel_helper(n_m, m, mode):
    # feedback_function(m, x, mode)
    # estimate(m, x, mode)
    return 1 - P_no_error(m, n_m, mode)

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# COMPUTE HISTOGRAM

if __name__ == '__main__':
    # unittest.main(verbosity=2)

    print("Simulating feedback without weighing on ideal code")
    Mmax = 9

    numBins = 500
    plotcut = 4
    plotList = [4, 5, 6, 7, 8]

    # initialize
    plt.close()

    delta_theta = np.zeros((Mmax, 2**Mmax))
    hist_array = np.zeros((4, Mmax, numBins))
    bin_array = np.zeros((4, Mmax, numBins+1))

    # compute
    for mode in np.arange(4):
        mode_name = ['rpe', 'arpe', 'off', 'random'][mode]
        P_array = [np.zeros(2**m) for m in np.arange(1, Mmax+1)]
        for m in np.arange(1, Mmax+1):
            # print(m)
            result_list = misc.result_range(0, 2**m, m)
            P_array[m-1] = misc.parallel_map_(_parallel_helper, result_list,
                                      task_args=(m, mode))
            hist, bins = np.histogram(np.clip(P_array[m-1], 0, 0.2),
                                      numBins, range=(0, 1), density=True)
            hist_array[mode, m-1] = hist/numBins
            bin_array[mode, m-1] = bins
        print(mode_name+' done')
    print('computing done')
    hist_array = hist_array.transpose(1, 0, 2)
    bin_array = bin_array.transpose(1, 0, 2)

    # initialize plotting
    fig, ax_array = plt.subplots(len(plotList), 4, sharex='col', sharey='row')
    fig.set_size_inches(8.75*2, 11)
    plt.rc('font', size=14, **{'family': 'sans-serif',
                               'sans-serif': ['Helvetica']})
    plt.rc('text', usetex=True)
    ylabels = [r"${0}$".format(0.1*x) for x in range(11)]
    xlabels = [r"${0}$".format(0.05*x) for x in range(5)]
    xlabels[-1] = r'$> 0.2$'

    # plot
    for mode in np.arange(4):
        for i, m in enumerate(plotList):
            bins = bin_array[m-1][mode]
            hist = hist_array[m-1][mode]
            widths = np.diff(bins)
            widths[100] = 0.03
            ax = ax_array[i][mode]
            ax.bar(bins[:-1], hist, widths, color="None")
            ax.set_xlim([0, 0.21])
            plt.xticks(0.05 * np.arange(5))
            plt.yticks(0.1 * np.arange(11))
            ax.set_xticklabels(xlabels)
            ax.set_yticklabels(ylabels)
            ax.set_ylim([0, 0.6])
            if mode == 0:
                ax.set_ylabel(r'$P$', rotation='horizontal')
                ax.yaxis.labelpad = 20
            if mode == 3:
                ax.yaxis.set_label_position("right")
                ax.set_ylabel(str(m), fontsize=16, rotation='horizontal')
                ax.yaxis.labelpad = 31
            if m == max(plotList):
                ax.set_xlabel(r'$P_{\mathrm{error,p}}^{\sqrt{\pi}/6}$')
    ax_array[0][0].set_title(r'RPE', fontsize=20)
    ax_array[0][1].set_title(r'ARPE', fontsize=20)
    ax_array[0][2].set_title(r'OFF', fontsize=20)
    ax_array[0][3].set_title(r'RAND', fontsize=20)
    plt.suptitle(r'M', fontsize=20, x=0.95, y=0.92)
    # fig.savefig('histograms2.eps')
    plt.show()
