# -*- coding: utf-8 -*-
import sys
import traceback
import warnings
from ..settings import DEBUG


def warn_with_traceback(message, category, filename, lineno, file=None, line=None):
    tb = traceback.format_stack()
    print('Warning detected and DEBUG=True, printing stack')
    log = file if hasattr(file, 'write') else sys.stderr
    log.write(warnings.formatwarning(message, category, filename, lineno, line))
    if len(tb) > 8:
        for i in range(3):
            print(tb[i])
        print('...\n')
        for i in range(-5, 0):
            print(tb[i])
    else:
        for line in tb:
            print(line)
    print('END OF STACK \n')

if DEBUG:
    warnings.showwarning = warn_with_traceback
