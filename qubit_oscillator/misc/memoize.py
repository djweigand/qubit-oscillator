import pickle


class Memoize:
    """Save Argument and Result of an expensive function in dictionary"""
    def __init__(self, f):
        self.f = f
        self.memo = {}

    def __call__(self, *args):
        key = hash(pickle.dumps(args, protocol=-1))
        result = self.memo.get(key)
        if result is None:
            result = (self.f(*args),)
            self.memo[key] = result
        return result[0]

    def reset(self):
        self.memo = {}
