# -*- coding: utf-8 -*-
import numpy as np
from scipy import interpolate
import os
import platform
import warnings


def bin_to_int(x):
    y = 0
    for i, j in enumerate(x):
        if j:
            y += 1 << i
    return y


def int_to_bin(int_array, m):
    return int_array[:, np.newaxis] >> np.arange(m)[::-1] & 1


def absmax(array):
    return np.abs(array).max()


def mask(choicelist, condlist, default=0):
    """
    Return an array of the same length as choicelist masked by condlist

    bit of a hack

    Args:
        choicelist: Array
        condlist: Boolean Array with same length as choicelist
        default: all entries j of choicelist with condlist[j] == False
            are set to default

    Returns:
        array
    """
    choicelist_list = [choicelist, choicelist]
    condlist_list = [condlist, condlist]
    return np.select(condlist_list, choicelist_list, default)


def shifted_mod(vec, scalar):
    return np.mod(vec + scalar / 2, scalar) - scalar / 2


def split_function(x, y, x_inter=None):
    """
    Take a perdiodic array x and split both x and y in chunks along the period

    As example let
    x = [1, 2, 3, 1.1, 2.1, 3.1]
    y = [1, 2, 3, 4, 5, 6]
    Then, the function returns
    [[1, 2, 3], [1.1, 2.1, 3.1]], [[1, 2, 3], [4, 5, 6]]

    Args:
        x: periodic array
        y: array of same length
    Returns:
        list of arrays x cut by period
        list of arrays y cut by period of x
    """
    dist = np.diff(x)
    cuts = np.where(dist < 0)[0] + 1
    xlist, ylist = np.split(x, cuts), np.split(y, cuts)
    if x_inter is None:
        return xlist, ylist

    else:
        counter = np.arange(len(xlist))
        f_list = [interpolate.interp1d(xlist[i], ylist[i],
                                   bounds_error=False,
                                   fill_value=0,
                                   assume_sorted=True) for i in counter]
        y_inter = np.asarray([f_list[i](x_inter) for i in counter])
        y_inter = np.sum(y_inter, axis=0)
        return y_inter


def wrap_function(x, y, x_inter):
    """
    Take a perdiodic array x, wrap y along the period, interpolate on x_inter

    As example let
    x = [1, 2, 3, 1, 2, 3]
    x_inter = [1, 1.5, 2, 2.5, 3]
    y = [1, 2, 1, 3, 4, 3]
    Then, the function returns
    [4, 5, 6, 5, 4]

    The function is set to 0 if x_inter is out of bounds. Thus, y_inter will
    be underestimated around the limits of x_inter
    Args:
        x: periodic array
        y: array of same length
        x_inter: linspace on one period of x
    Returns:
        array of length x_inter
    """
    return split_function(x, y, x_inter=x_inter).sum(axis=0)


def apply_nd_argsort(array, index, axis=-1):
    idx = list(np.ix_(*[np.arange(i) for i in array.shape]))
    idx[axis] = index
    return array[idx]


def seconds_to_hh_mm_ss(time):
    time_m = 0
    time_h = 0
    time_m, time_s = divmod(time, 60)
    time_h, time_m = divmod(time_m, 60)
    return (int(time_h), int(time_m), time_s)


def alarm(time=1, tone=443):
    os.system('play --no-show-progress --null --channels 1 synth {} sine {}'.format(time, tone))


if platform.system() == 'Windows':
    def memory_limit(lim_GB):
        warnings.warn("resource package not supported on Windows, memory limit is NOT SET!")
else:
    import resource
    def memory_limit(lim_GB):
        resource.setrlimit(resource.RLIMIT_AS, (lim_GB * 1024**3, resource.RLIM_INFINITY))
