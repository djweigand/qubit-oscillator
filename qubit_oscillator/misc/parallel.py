# -*- coding: utf-8 -*-
import warnings
import time
import sys
import math
import itertools
import psutil
from multiprocessing import Pool
from ..settings import PARALLEL, PARALLEL_PROGRESS_BAR, DEBUG_CLUSTER
from .other import seconds_to_hh_mm_ss

if hasattr(sys, '_called_from_test') and PARALLEL not in ('local', 'serial'):
    warnings.warn('Called from test with non-local parallelization. '
                  + 'Switching to local.')
    PARALLEL = 'local'
elif PARALLEL == 'lxt':
    import lxt_cluster.frontend as cluster

if PARALLEL == 'MPI':
    from mpi4py import MPI
    _comm = MPI.COMM_WORLD
    _size = _comm.Get_size()
    _rank = _comm.Get_rank()
else:
    _rank = 0
    _comm = None
    _size = 1


class ProgressBar():
    def __init__(self, nr_tasks, step=0.001):
        self.nr_tasks = nr_tasks
        self.start_time = time.time()
        self.print_threshold = 0
        self.passed = 0
        self.step = step

    def update(self, n_current):
        done_tasks = n_current / self.nr_tasks
        if done_tasks > self.print_threshold:
            self.passed = time.time() - self.start_time
            self.print_threshold += self.step
            print_("{:.2%} , Passed: {:2d}:{:2d}:{:4.2f},  ETA: {:2d}:{:2d}:{:4.2f}     ".format(
                                *((done_tasks,)
                                + seconds_to_hh_mm_ss(self.passed)
                                + seconds_to_hh_mm_ss(self.passed * (1 / done_tasks - 1)))),
                                end="\r")

    def finished(self):
        self.passed = time.time() - self.start_time
        print_("{} tasks done in {:2d}:{:2d}:{:4.2f}".format(*((self.nr_tasks,) + seconds_to_hh_mm_ss(self.passed))))


class NoProgressBar():
    def update(self, *args, **kwargs):
        pass

    def finished(self, *args, **kwargs):
        pass


def print_(*args, **kwargs):
    if _rank == 0:
        print(*args, **kwargs)


def parallel_map(task, values, task_args=tuple(), task_kwargs={}, serial=False,
                 local=False, **kwargs):
    if (PARALLEL_PROGRESS_BAR is not None) and ("progress_bar" not in kwargs):
        kwargs["progress_bar"] = PARALLEL_PROGRESS_BAR

    if serial or PARALLEL == 'serial':
        return _serial_map(task, values, task_args, task_kwargs, **kwargs)
    elif (local or PARALLEL == 'local'):
        return _parallel_map(task, values, task_args, task_kwargs, **kwargs)
    elif PARALLEL == 'MPI':
        return mpi_map(task, values, task_args, task_kwargs)
    elif PARALLEL == 'lxt':
        if "verbose" not in kwargs:
            if DEBUG_CLUSTER:
                kwargs["verbose"] = DEBUG_CLUSTER
            else:
                kwargs["verbose"] = 0
        return cluster.cluster_map(task, values, task_args, task_kwargs,
                                   **kwargs)
    else:
        raise Exception('Bad config for misc.parallel_map')

# Alias to make writing stuff easier without breaking backwards compatibility
parallel_map_ = parallel_map


def patches(iterable, n=1):
    L = len(iterable)
    step = math.ceil(L / n)       # Python3 division is always float
    return [iterable[i * step:min((i + 1) * step, L)] for i in range(n)]


def mpi_map(task, values, task_args=tuple(), task_kwargs={}):
    idx = range(len(values))
    idx = patches(idx, _size)
    idx_local = idx[_rank]
    result_local = [task(*((values[i],) + task_args), **task_kwargs)
                    for i in idx_local]
    result = _comm.allgather(result_local)
    result = list(itertools.chain.from_iterable(result))
    return result


def _serial_map(task, values, task_args=tuple(), task_kwargs={}, **kwargs):
    """
    Serial mapping function with the same call signature as parallel_map, for
    easy switching between serial and parallel execution. This
    is functionally equivalent to:

        result = [task(value, *task_args, **task_kwargs) for value in values]

    This function work as a drop-in replacement of :func:`_parallel_map`.
    Modified version of the qutip function.

    Parameters
    ----------

    task: a Python function
        The function that is to be called for each value in ``task_vec``.

    values: array / list
        The list or array of values for which the ``task`` function is to be
        evaluated.

    task_args: list / dictionary
        The optional additional argument to the ``task`` function.

    task_kwargs: list / dictionary
        The optional additional keyword argument to the ``task`` function.

    progress_bar: ProgressBar
        Progress bar class instance for showing progress.

    Returns
    --------
    result : list
        The result list contains the value of
        ``task(value, *task_args, **task_kwargs)`` for each
        value in ``values``.
    """
    try:
        progress_bar = kwargs['progress_bar']
    except:
        progress_bar = False
    if progress_bar:
        if progress_bar is True:
            progress_bar = ProgressBar(len(values), 0.05)
        else:
            progress_bar = ProgressBar(len(values), progress_bar)
    else:
        progress_bar = NoProgressBar()

    results = []
    for n, value in enumerate(values):
        progress_bar.update(n)
        result = task(value, *task_args, **task_kwargs)
        results.append(result)
    progress_bar.finished()

    return results


def _parallel_map(task, values, task_args=tuple(), task_kwargs={}, **kwargs):
    """
    Parallel execution of a mapping of `values` to the function `task`. This
    is functionally equivalent to:

        result = [task(value, *task_args, **task_kwargs) for value in values]

    Modified version of the qutip function.

    Parameters
    ----------

    task: a Python function
        The function that is to be called for each value in ``task_vec``.

    values: array / list
        The list or array of values for which the ``task`` function is to be
        evaluated.

    task_args: list / dictionary
        The optional additional argument to the ``task`` function.

    task_kwargs: list / dictionary
        The optional additional keyword argument to the ``task`` function.

    progress_bar: ProgressBar
        Progress bar class instance for showing progress.

    Returns
    --------
    result : list
        The result list contains the value of
        ``task(value, *task_args, **task_kwargs)`` for each
        value in ``values``.

    """
    kw = {'num_cpus': psutil.cpu_count(logical=False)}
    if 'num_cpus' in kwargs:
        kw['num_cpus'] = kwargs['num_cpus']

    try:
        progress_bar = kwargs['progress_bar']
    except:
        progress_bar = False
    if progress_bar:
        if progress_bar is True:
            progress_bar = ProgressBar(len(values), 0.05)
        else:
            progress_bar = ProgressBar(len(values), progress_bar)
    else:
        progress_bar = NoProgressBar()

    nfinished = [0]

    def _update_progress_bar(x):
        nfinished[0] += 1
        progress_bar.update(nfinished[0])

    try:
        pool = Pool(processes=kw['num_cpus'])
        
        async_res = [pool.apply_async(task, (value,) + task_args, task_kwargs,
                                      _update_progress_bar)
                     for value in values]

        while not all([ar.ready() for ar in async_res]):
            for ar in async_res:
                ar.wait(timeout=0.1)

        pool.terminate()
        pool.join()

    except KeyboardInterrupt as e:
        pool.terminate()
        pool.join()
        raise e

    progress_bar.finished()

    return [ar.get() for ar in async_res]
