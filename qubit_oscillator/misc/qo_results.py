# -*- coding: utf-8 -*-
import numpy as np
import itertools
from .parallel import parallel_map
from .other import int_to_bin
from ..settings import (N)
from qutip import (displace, squeeze, basis)


def result_range(Min, Max, m):
    """Return range min-max as bit strings of length m"""
    results = np.arange(Min, Max)
    return int_to_bin(results, m)


def result_random(N_results, m):
    """Return N_results random bit-strings of length m"""
    results = np.random.choice(2**m, size=N_results, replace=False)
    return int_to_bin(results, m)


def error_range(weight, m, minweight=0):
    error_list = []
    for w in range(minweight, weight + 1):
        combinations = itertools.combinations(range(m), w)
        for combination in combinations:
            error = np.zeros(m, dtype=np.int)
            for index in combination:
                error[index] = 1
            error_list.append(error)
    return error_list


def code_displacement(s, delta=0.2):
    return np.exp(-2 * delta**2 * s**2 * np.pi) * displace(N, s * np.sqrt(2 * np.pi))


def approximate_code_state(delta=0.2):
    r = np.log(1 / delta)
    squeezed_vac = squeeze(N, r) * basis(N, 0)
    displacements = np.asarray(parallel_map(code_displacement,
                                            np.arange(-10, 11),
                                            task_kwargs={"delta": delta}))
    state = (np.sum(displacements, axis=0) * squeezed_vac).unit()
    return state
