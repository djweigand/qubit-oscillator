# -*- coding: utf-8 -*-
import numpy as np
from .other import apply_nd_argsort, mask
from .memoize import Memoize
import warnings


@Memoize
def gauss(x, sigma):
    f = np.exp(-x**2 / (2 * sigma**2))
    return f / np.sum(f)


def moment(x_vec, func, order=None, mean=0, total=None):
    if total is None:
        total = np.sum(func)

    if order is not None:
        return np.sum((x_vec - mean)**order * func) / total

    else:
        mean = np.sum(x_vec * func) / total
        moment_list = [mean]
        moment_list.append(np.sum((x_vec - mean)**2 * func) / total)
        moment_list.append(np.sum((x_vec - mean)**3 * func) / total)
        moment_list.append(np.sum((x_vec - mean)**4 * func) / total)
        return total, moment_list


def weighted_avg_and_std(values, weights=None, axis=None):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.
    """
    # Enable broadcasting
    values = np.asanyarray(values)
    weights = np.asanyarray(weights)
    if weights.dtype != np.float_:
        warnings.warn('Dtype of weights is not float, fixing that')
        weights = np.float_(weights)
    if weights is not None:
        weights += np.finfo(float).eps
    if axis is not None:
        shape = list(values.shape)
        shape[axis] = 1
    else:
        shape = 1
    average = np.average(values, weights=weights, axis=axis)
    variance = np.average((values - average.reshape(shape))**2, weights=weights, axis=axis)
    return (average, np.sqrt(variance))


def two_sided_weighted_avg_and_std(values, weights=None, axis=None):
    """
    Return the weighted average and standard deviation.

    values, weights -- Numpy ndarrays with the same shape.
    """
    # Enable broadcasting
    values = np.asanyarray(values)
    weights = np.asanyarray(weights)
    if weights.dtype != np.float_:
        warnings.warn('Dtype of weights is not float, fixing that')
        weights = np.float_(weights)
    if weights is not None:
        weights += np.finfo(float).eps
    if axis is not None:
        shape = list(values.shape)
        shape[axis] = 1
    else:
        shape = 1
    average = np.average(values, weights=weights, axis=axis)
    va = (values - average.reshape(shape))**2
    variance = np.average(va, weights=weights, axis=axis)
    w_p = np.copy(weights)
    w_p[values - average.reshape(shape) < 0] = np.finfo(float).eps
    w_n = np.copy(weights)
    w_n[values - average.reshape(shape) > 0] = np.finfo(float).eps
    var_pos = np.average(va, weights=w_p, axis=axis)
    var_neg = np.average(va, weights=w_n, axis=axis)
    return (average, np.sqrt(variance), [np.sqrt(var_pos), np.sqrt(var_neg)])


def circular_weighed_avg_and_std(values, weights=None):
    # See wikipedia, circular mean for the reasoning
    average_s = np.average(np.sin(values), weights=weights)
    average_c = np.average(np.cos(values), weights=weights)
    average = np.arctan2(average_s, average_c)
    # Reshift the values around the mean
    values -= average
    values = ((values + np.pi) % (2 * np.pi) - np.pi)
    variance = np.average((values)**2, weights=weights)
    return (average, np.sqrt(variance))


def arg_weighted_quantile(a, q, axis=None, weights=None, sorted=False):
    """ Very close to np.percentile, but supports weights.
    NOTE: q should be in [0, 100]!
    :param a: np.array with data
    :param q: array-like with many quantiles needed
    :param weights: array-like of the same length as `array`
    :param values_sorted: bool, if True, then will avoid sorting of initial array
    :param old_style: if True, will correct output to be consistent with np.percentile.
    :return: np.array with computed q.
    """
    a = np.array(a)
    q = np.array(q, dtype=np.float)

    # taken from numpy source, should be fastest solution
    if q.ndim == 0:
        # Do not allow 0-d arrays because following code fails for scalar
        q = q[None]     # put q into 1D array
    if q.size < 10:
        for i in range(q.size):
            if q[i] < 0. or q[i] > 100.:
                raise ValueError("Percentiles must be in the range [0,100]")
            q[i] /= 100.
    else:
        # faster than any()
        if np.count_nonzero(q < 0.) or np.count_nonzero(q > 100.):
            raise ValueError("Percentiles must be in the range [0,100]")
        q /= 100.
    # end of numpy code
    if weights is None:
        weights = np.ones_like(a, dtype=np.float)
    else:
        weights = np.array(weights, dtype=np.float)

    if not sorted:
        sorter = np.argsort(a, axis=axis)
        a = apply_nd_argsort(a, sorter, axis=axis)
        weights = apply_nd_argsort(weights, sorter, axis=axis)
    weights_c = np.cumsum(weights, axis=axis)
    weights_last = np.take(weights_c, -1, axis=axis)
    if axis == 0:
        shape_target = (1, -1)
    else:
        shape_target = (-1, 1)
    weights_c = weights_c / weights_last.reshape(shape_target)
    idx = np.apply_along_axis(np.searchsorted, axis, weights_c, q)
    return a, weights, idx


def weighted_quantile(a, q, axis=None, weights=None, sorted=False):
    """ Very close to np.percentile, but supports weights.
    NOTE: q should be in [0, 100]!
    :param a: np.array with data
    :param q: array-like with many quantiles needed
    :param weights: array-like of the same length as `array`
    :param values_sorted: bool, if True, then will avoid sorting of initial array
    :param old_style: if True, will correct output to be consistent with np.percentile.
    :return: np.array with computed q.
    """
    if axis is None:
        a = a.ravel()
    if weights is not None:
        weights = weights.ravel()
    a, _, idx = arg_weighted_quantile(a, q, axis=axis, weights=weights, sorted=sorted)
    return apply_nd_argsort(a, idx, axis=axis).ravel()


def heralding(p, w, t, axis=-1, cut='right', return_mask=False):
    assert len(p.shape) == 2
    assert p.shape == w.shape
    if axis == 0:
        p = p.T
        w = w.T
    idx = np.argsort(p, axis=-1)
    p = apply_nd_argsort(p, idx, axis=-1)
    w = apply_nd_argsort(w, idx, axis=-1)
    cum = np.cumsum(w, axis=-1)
    cum_N = cum / cum.max(axis=-1).reshape(-1, 1)
    idx = np.apply_along_axis(np.searchsorted, -1, cum_N, t)
    helper = np.arange(p.shape[-1]).reshape(1, -1).repeat(p.shape[0], axis=0)
    if cut == 'right':
        m = np.array([helper[i] <= idx[i] for i in range(p.shape[0])])
    elif cut == 'left':
        m = np.array([helper[i] < idx[i] for i in range(p.shape[0])])
    p = mask(p, m, default=1)
    w = mask(w, m, default=0)
    if axis == 0:
        p = p.T
        w = w.T
        m = m.T
    if return_mask:
        return p, w, m
    else:
        return p, w


def S_discrete(data, weights):
    data = np.asarray(data)
    return np.abs(np.sum(np.exp(1j * data) * weights)) / np.sum(weights)
