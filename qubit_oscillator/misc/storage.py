# -*- coding: utf-8 -*-
import shutil
import warnings
import pickle
from .parallel import _rank, _comm
from ..settings import PARALLEL


def backup(path):
    if _rank == 0:
        try:
            shutil.copy2(path, path + '.backup')
        except FileNotFoundError:
            print('\n')
            warnings.warn('Data file not found, check if that is correct')
            print('\n')
            pass


def load(path, print_keys=False, key_legend=None):
    data = {}
    if _rank == 0:
        try:
            with open(path, 'rb') as f:
                data = pickle.load(f)
                keys = list(data.keys())
                if print_keys:
                    print('data loaded:')
                    if key_legend is not None:
                        print('Legend:')
                        print(key_legend)
                    print('Keys:')
                    for key in keys:
                        stored_len = len(data[key][-1])
                        print(str(key) + ' ' + str(stored_len))
                else:
                    print('data loaded')
        except FileNotFoundError:
            warnings.warn('data not yet stored, intializing empty')
            data = {}
            keys = []
    if PARALLEL == 'MPI':
        data = _comm.bcast(data)
    keys = list(data.keys())
    return data, keys


def store(path, data):
    if _rank == 0:
        with open(path, 'wb') as f:
            pickle.dump(data, f, protocol=-1)
            print('data saved!')
