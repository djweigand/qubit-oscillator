import numpy as np
import itertools
from scipy.special import binom
from ..misc import Memoize
import warnings

from qubit_oscillator.constants import EIGENVALUE_SCALE


class Memoize_Breeding(Memoize):
    def __call__(self, encoding, exp_info):

        key = (encoding.m, encoding.mode, exp_info.measured_op, exp_info.delta)
        result = self.memo.get(key)
        if result is None:
            result = self.f(*key)
            phases, weight = result
            for i in range(len(phases)):
                self.memo[(i, encoding.mode, exp_info.measured_op, exp_info.delta)] = phases[i]
            return weight
        else:
            return result

_N_points = 5000
_LIM = 15
_Q = np.linspace(-_LIM, _LIM, _N_points)
_Q_GAUSS = np.exp(-_Q**2)
_Q_GAUSS /= np.sum(_Q_GAUSS)
_IDX = np.arange(_N_points)


def _P_breeding(phases, q_dist, p_dist):
    m = len(phases)
    s = np.arange(-m / 2, m / 2 + 1)
    # compute weights of state ingoing at mode 1
    if len(phases) == 0:
        weights = np.array(1)
    else:
        weights = []
        for i in np.arange(m + 1):
            n_comb = np.int(binom(m, i))
            comb = itertools.chain.from_iterable(itertools.combinations(phases, i))
            comb = np.fromiter(comb, dtype=np.complex_, count=n_comb * i)
            comb.resize((n_comb, i))
            comb = np.exp(1j * comb.sum(axis=1))
            weights.append(comb.sum())
        weights = np.array(weights)
    t = 1j * q_dist
    s = s.reshape((1, -1, 1))
    s = s + np.array([1 / 2, -1 / 2]).reshape((1, 1, -1))
    t = t * s
    t = t * _Q.reshape((-1, 1, 1))

    np.exp(t, out=t)
    f = weights.reshape((1, -1, 1)) * t
    f = np.sum(f, axis=(1, 2))
    np.abs(f, out=f)
    np.power(f, 2, out=f)
    f = np.real(f)
    if p_dist == 1:
        f *= _Q_GAUSS
    else:
        f *= np.exp(-_Q**2 * p_dist**2)
    f /= np.sum(f)
    return f


def _P_breeding_C(q_dist, p_dist):
    t = 1j * q_dist * np.array([1 / 2, -1 / 2]).reshape((1, -1))
    t = t * _Q.reshape((-1, 1))
    np.exp(t, out=t)
    f = np.sum(t, axis=1)
    np.abs(f, out=f)
    np.power(f, 2, out=f)
    f = np.real(f)
    if p_dist == 1:
        f *= _Q_GAUSS
    else:
        f *= np.exp(-_Q**2 * p_dist**2)
    f /= np.sum(f)
    return f


@Memoize_Breeding
def feedback_breeding(m_max, mode, measured_op, delta):
    phases = np.zeros(m_max)
    mask = np.zeros(m_max, dtype=bool)
    weight = 1
    measured_dist = EIGENVALUE_SCALE[measured_op]
    for i in range(m_max):
        if mode == 'cat_breeding_B':
            q_dist = measured_dist * np.sqrt(2)**(m_max - i)
            p_dist = delta
            weights_current = _P_breeding(phases[mask], q_dist, p_dist)
        elif mode == 'cat_breeding_BS':
            q_dist = measured_dist
            p_dist = delta * np.sqrt(2)**(i - m_max)
            weights_current = _P_breeding(phases[mask], q_dist, p_dist)
        elif mode == 'cat_breeding_C':
            q_dist = measured_dist
            p_dist = delta
            weights_current = _P_breeding_C(q_dist, p_dist)
        idx_current = np.random.choice(_IDX, p=weights_current)
        q_current = _Q[idx_current]
        if mode == 'cat_breeding_B':
            phases[i] = -1 * q_dist * q_current
            phases -= mask * phases[i]
        elif mode == 'cat_breeding_BS':
            phases[i] = -1 * q_dist * q_current
            phases -= mask * phases[i]
        elif mode == 'cat_breeding_C':
            phases[i] = -1 * measured_dist * q_current
        mask[i] = 1
        weight *= weights_current[idx_current]
    #warnings.warn(str(phases))
    return phases, weight
