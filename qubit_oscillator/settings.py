# -*- coding: utf-8 -*-

"""
Convention: All constants defined here are UPPER CASE
"""
import configparser
import numpy as np

# Needed so that plots work via ssh
#import matplotlib
#atplotlib.use('Agg')

config = configparser.ConfigParser()
config.read('qubit_oscillator.cfg')

# Choices are: serial, local, lxt, MPI
PARALLEL = config.get('parallel', 'parallel', fallback='local')
#PARALLEL_PROGRESS_BAR = config.getboolean('parallel', 'progress_bar',
#                                          fallback=None)
PARALLEL_PROGRESS_BAR = config.getfloat('parallel', 'progress_bar', fallback=None)

# -----------------------------------
# Debugging
DEBUG = config.getboolean('debug', 'debug', fallback=False)

# Force cluster debug output
DEBUG_CLUSTER = config.getint('debug', 'debug_cluster', fallback=0)
# 0: off, 1: on error, 2: full
DEBUG_PLOT = config.getint('debug', 'debug_plot', fallback=0)
DEBUG_RAISE = config.getboolean('debug', 'debug_raise', fallback=False)

# -----------------------------------
# Folders
DEBUG_PLOT_PATH = config.get('folders', 'debug_plot_path', fallback="../DEBUG/")
PLOT_PATH = config.get('folders', 'plot_path', fallback="../FIGURES/")
DATA_PATH = config.get('folders', 'data_path', fallback="../DATA/")

# -----------------------------------
# Qutip configuration
FIT_DELTA = config.getboolean('simulation', 'fit_delta', fallback=False)
TIDYUP = config.getboolean('simulation', 'tidyup', fallback=False)
N = config.getint('simulation', 'N', fallback=400)

# System Properties
CAT_BREEDING = config.getboolean('system', 'cat_breeding', fallback=False)
MEASURED_OPS = config.get('system', 'measured_ops', fallback='S_p S_q X Z M_p M_q').split()
CHI = config.getfloat('system', 'chi_mhz', fallback=2.4) * 10**(-3) * 2 * np.pi
KAPPA = config.getfloat('system', 'kappa_khz', fallback=1) * 10**(-6) * 2 * np.pi
DELTA = config.getfloat('system', 'delta', fallback=0.14)
T_MEASURE = config.getfloat('system', 't_meas', fallback=150)
FEEDBACK_CHOICES = 20

# Default error rate assumed by feedback
ERROR_RATE = config.getfloat('system', 'error_rate', fallback=0.)
ERROR_WEIGHT_FEEDBACK = config.getint('system', 'error_weight_fb', fallback=2)
ERROR_WEIGHT_ANALYSIS = config.getint('system', 'error_weight_fb', fallback=2)
