from qubit_oscillator.constants import STABILIZERS
from qubit_oscillator.settings import (DEBUG, DEBUG_PLOT, DEBUG_RAISE, FIT_DELTA,
                                       DELTA, N)
from qubit_oscillator.constants import *

if not __name__ == '__main__' and DEBUG_PLOT:
    import matplotlib.pyplot as plt

import time                                                   # used for timing
start_time = time.time()
import scipy.integrate
import scipy.optimize as opt
import numpy as np
import qutip as qt
import warnings


import qubit_oscillator.misc as misc
import qubit_oscillator.simulation.shared as shared

eigenvalue_std = shared.eigenvalue_std


def wrapper(states, n_points_plot=1024,
                      threshold=np.sqrt(np.pi) / 6, period=np.sqrt(np.pi),
                      progress_bar=False, measured_op='S_p',
                      correcting_alpha=0, M=None, **kwargs):
    """
    Return interesting functions obtained from the Wigner function of a state.

    Args:
        states: list of Qutip state objects
        lim: float, the vectro x given to qutip.wigner() is in (-lim,lim)
        n_points_plot: Integer, Number of Points used in the vectors x and y.
        threshold: Threshold used to compute the error rate

    Returns:
        x_vec: np array, linspace of points in x
        y_vec: np array, linspace of points in y
        W: Wigner function at points x,y
        qfunc: Integral of W over p
        pfunc: Integral of W over q
        pfunc_m: pfunc, masked to (-threshold,threshold) mod sqrt(pi)
        qfunc_m: qfunc, masked to (-threshold,threshold) mod sqrt(pi)
        p_z: rate of v>threshold (z-type) errors
        p_x: rate of u>threshold (z-type) errors
        error_rate: combined rate
    """
    key_list = ['p_z', 'p_x', 'fit_delta', 'variance', 'sharpness']
    for key in key_list:
        if key not in kwargs:
            kwargs[key] = False
    if type(states) is qt.Qobj:
        states = [states]
        kwargs['serial'] = True
        progress_bar = False
    if kwargs['sharpness']:
        if kwargs['p_z']:
            p = misc.parallel_map(_sharpness, states, task_args=('p_z',), **kwargs)
        elif kwargs['p_x']:
            p = misc.parallel_map(_sharpness, states, task_args=('p_x',), **kwargs)
        return np.sqrt((np.abs(p)**(-2) - 1)) / (2 * np.sqrt(np.pi))
    if kwargs['fit_delta']:
        n_points_plot = 2048
    if True:
        x_vec, y_vec, w, qfunc, pfunc, qfunc_m, pfunc_m, p_z, p_x, error_rate = \
            _parallelize_wrapper(states, n_points_plot,
                             threshold, period, progress_bar,
                             correcting_alpha, M, **kwargs)
    if kwargs['p_z']:
        if kwargs['fit_delta']:
            p_z = misc.parallel_map_(opt_delta, w,
                                    task_args=(x_vec, y_vec, M), **kwargs)
        return np.asarray(p_z)
    elif kwargs['p_x']:
        if kwargs['fit_delta']:
            raise NotImplementedError
        return np.asarray(p_x)
    else:
        return map(np.asarray, (x_vec, y_vec, w,
                                   qfunc, pfunc, qfunc_m, pfunc_m,
                                   p_z, p_x, error_rate))


def _parallelize_wrapper(states, n_points_plot,
                      threshold, period, progress_bar,
                      correcting_alpha, M, **kwargs):
    """
    Return interesting functions obtained from the Wigner function of a state.

    Args:
        states: list of Qutip state objects
        lim: float, the vector x given to qutip.wigner() is in (-lim,lim)
        n_points_plot: Integer, Number of Points used in the vectors x and y.
        threshold: Threshold used to compute the error rate

    Returns:
        x_vec: np array, linspace of points in x
        y_vec: np array, linspace of points in y
        W: Wigner function at points x,y
        qfunc: Integral of W over p
        pfunc: Integral of W over q
        pfunc_m: pfunc, masked to (-threshold,threshold) mod sqrt(pi)
        qfunc_m: qfunc, masked to (-threshold,threshold) mod sqrt(pi)
        p_z: rate of v>threshold (z-type) errors
        p_x: rate of u>threshold (z-type) errors
        error_rate: combined rate
    """
    kwargs['correcting_alpha'] = correcting_alpha

    lim = 20 * np.sqrt(n_points_plot / 1024)
    x_vec = np.linspace(-lim, lim, n_points_plot)
    map_result = misc.parallel_map_(qt.wigner, states,
                                    task_args=(x_vec, x_vec),
                                    task_kwargs={'method': 'fft'},
                                    progress_bar=progress_bar,
                                    verbose=0,
                                    **kwargs)
    W, yvecs = list(zip(*map_result))
    xvecs = np.repeat([x_vec, x_vec], [1, len(states) - 1], axis=0)
    y_vec = yvecs[-1]

    qfunc, pfunc, qfunc_m, pfunc_m, p_z, p_x, error_rate = list(zip(
        *misc.parallel_map_(analysis_function,
                    list(zip(*[W, xvecs, yvecs])),
                    task_args=(threshold, period),
                    task_kwargs=kwargs,
                    **kwargs
                    # This is usually very fast (<1s)
                    # progress_bar = progress_bar
                    )))
    return map(np.asarray, (x_vec, y_vec, W, qfunc, pfunc, qfunc_m, pfunc_m,
                            p_z, p_x, error_rate))


# Optimized
def analysis_function(W_tuple, threshold, period, **kwargs):
    """
    Return interesting functions obtained from the Wigner function of a state.

    Args:
        W_tuple: tuple of W, x_vec, y_vec as defined in analysis_wrapper
        threshold: Threshold used to compute the error rate

    Returns:
        qfunc: Integral of W over p
        pfunc: Integral of W over q
        pfunc_m: pfunc, masked to (-threshold,threshold) mod sqrt(pi)
        qfunc_m: qfunc, masked to (-threshold,threshold) mod sqrt(pi)
        p_z: rate of v>threshold (z-type) errors
        p_x: rate of u>threshold (z-type) errors
        error_rate: combined rate
    """
    correcting_alpha = kwargs['correcting_alpha']
    correcting_x = correcting_alpha.real
    correcting_z = correcting_alpha.imag
    pfunc = None
    pfunc_m = None
    qfunc = None
    qfunc_m = None
    p_z = None
    p_x = None
    error_rate = None

    w = W_tuple[0]
    x_vec = W_tuple[1]
    y_vec = W_tuple[2]

    dx = np.mean([np.diff(x_vec).max(), np.diff(x_vec).min()])
    dy = np.mean([np.diff(y_vec).max(), np.diff(y_vec).min()])

    # Using first romb and then simps for the integrals gives the best performance
    # I use simps in both cases anyway, as the fft in wigner is faster if len(vec)
    # is a power of 2, which is incompatible with romb
    if not kwargs['p_x']:
        pfunc = scipy.integrate.simps(w, dx=dx, axis=1)
        pfunc_m, p_z = rate_1d(y_vec, pfunc, correcting_z, threshold, period, full=True)
        if kwargs['variance']:
            y_vec_periodic = misc.shifted_mod(y_vec + correcting_z, period)
            y_vec_periodic *= 2 * np.sqrt(np.pi)
            np.power(y_vec_periodic, 2, out=y_vec_periodic)
            y_vec_periodic *= pfunc_m
            p_z = np.sqrt(scipy.integrate.simps(y_vec_periodic, y_vec) / np.pi)
        if kwargs['sharpness']:
            y_vec_periodic = misc.shifted_mod(y_vec + correcting_z, period)
            y_vec_periodic = y_vec_periodic * 1j * 2 * np.sqrt(np.pi)
            np.exp(y_vec_periodic, out=y_vec_periodic)
            y_vec_periodic *= pfunc_m
            p_z = scipy.integrate.simps(y_vec_periodic, y_vec)
            p_z = np.sqrt((np.abs(p_z)**(-2) - 1) / np.pi) / (2 * np.pi)
    if not kwargs['p_z']:
        qfunc = scipy.integrate.simps(w, dx=dy, axis=0)
        qfunc_m, p_x = rate_1d(x_vec, qfunc, correcting_x, threshold, period, full=True)
    if DEBUG or ('test' in kwargs and kwargs['test']):
        error = 1 - scipy.integrate.simps(pfunc, dx=dy)
        if error > 2 * 10**(-3):
            message = ("rounding error of Wigner function is large:"
                       + "%6f, %4f points, range 2* %f" % (error, len(x_vec), x_vec.max()))
            if DEBUG_PLOT:
                plt.close()
                plt.contourf(w)
                plt.savefig("./DEBUG/" + str(int(time.time())) + " bad wigner.png")
                plt.close()
            if DEBUG_RAISE:
                raise Exception(message)
            else:
                warnings.warn(message)
    if kwargs['p_z'] or kwargs['p_x']:
        pass
    else:
        error_rate = 1 - (1 - p_x) * (1 - p_z)
    return qfunc, pfunc, qfunc_m, pfunc_m, p_z, p_x, error_rate


def _sharpness(state, errortype):
    if errortype == 'p_z':
        stabilizer = 'S_p'
    elif errortype == 'p_x':
        stabilizer = 'S_q'
    return qt.expect(STABILIZERS[stabilizer], state)


def rate_1d(vec, func, correct, threshold, period, full=False):
    condlist = np.abs(misc.shifted_mod(vec + correct, period)) < threshold
    func_m = misc.mask(func, condlist)
    p = 1 - scipy.integrate.simps(func_m, vec)
    if full:
        return func_m, p
    else:
        return p


# NOTE: takes 10-20s on import:
if FIT_DELTA:
    _STATE0 = qt.squeeze(N, np.log(1 / DELTA)) * qt.basis(N, 0)

    def _squeezed_state_parallelizer(s):
        return (s, qt.displace(N, s * np.sqrt(np.pi / 2)) * _STATE0)

    #_SQUEEZED_STATES = [_squeezed_state_parallelizer(s) for s in range(-16, 16+1)]
    #_SQUEEZED_STATES = misc.parallel_map(_squeezed_state_parallelizer, range(-16, 16+1))
    #_SQUEEZED_STATES = {value[0]: value[1] for value in _SQUEEZED_STATES}
    _SQUEEZED_STATES = {s: _squeezed_state_parallelizer(s)[1] for s in range(-16, 16 + 1)}


def opt_delta(w, x_vec, y_vec, M):
    assert M is not None
    assert M < 17
    #s_range = range(-M, M+1, 2)
    #xopt = opt.fminbound(_diff, 10**(-9), 1, args=(w, x_vec, y_vec, s_range))
    #fval = -_diff(xopt, w, x_vec, y_vec, s_range)

    if M % 2 == 0:
        s_range = range(-16, 16 + 1, 2)
    else:
        s_range = range(-15, 15 + 1, 2)
    xopt = opt.fminbound(_diff, 10**(-12), 1, args=(w, x_vec, y_vec, s_range))
    fval = -_diff(xopt, w, x_vec, y_vec, s_range)
    if fval < 0.95:
        xopt = 1
    return xopt, fval


def _diff(delta, w, x_vec, y_vec, s_range):
    w_approx = wigner_codestate(x_vec, delta, s_range)
    diff = shared.overlap(w, w_approx, x_vec, y_vec)
    return -diff


def wigner_codestate(x, delta, s_range):
    state = 0
    for s in s_range:
        weight = np.exp(-0.5 * np.pi * delta**2 * s**2)
        state = state + weight * _SQUEEZED_STATES[s]
    state = state.unit()
    w, y = qt.wigner(state, x, x, method='fft')
    return w


def wigner_squeezed(x, delta):
    r = np.log(1 / delta[0])
    state = qt.squeeze(N, r) * qt.basis(N, 0)
    w, y = qt.wigner(state, x, x, method='fft')
    return w


















def gauss(x, a, x0, sigma):
    return a * np.exp(-(x - x0)**2 / (2 * sigma**2))


def fit_delta(x, y, period=np.sqrt(np.pi), correcting_z=0):
    x_periodic = misc.shifted_mod(x + correcting_z, np.sqrt(np.pi))
    x_split, y_split = misc.split_function(x_periodic, y)
    nr_patches = len(x_split)

    fit_list = [[] for _ in range(nr_patches)]
    weight_list = [0 for _ in range(nr_patches)]
    for i in range(nr_patches):
        x = x_split[i]
        y = y_split[i]
        #estimate mean and standard deviation
        #do the fit!
        popt, pcov = opt.curve_fit(gauss, x, y, p0=[1, 0, DELTA])
        weight_list[i] = np.sum(y)
        fit_list[i] = popt
    fit = np.asarray(fit_list)
    weights = np.asarray(weight_list)
    fit, var = misc.weighted_avg_and_std(fit, weights=weights, axis=0)
    return fit


def fit_delta_1d(x_vec, func, period=np.sqrt(np.pi), correcting_z=0):
    x_periodic = misc.shifted_mod(x_vec + correcting_z, np.sqrt(np.pi))
    x_split, func_split = misc.split_function(x_periodic, func)
    nr_patches = len(x_split)

    moment_list = [[] for _ in range(nr_patches)]
    weight_list = [0 for _ in range(nr_patches)]
    for i in range(nr_patches):
        x = x_split[i]
        f = func_split[i]
        weight_list[i], moment_list[i] = misc.moment(x, f)
    moments = np.asarray(moment_list)
    weights = np.asarray(weight_list)
    moments = misc.weighted_avg_and_std(moments, weights=weights, axis=0)
    #moments = np.max(moments, axis=0)
    return moments
