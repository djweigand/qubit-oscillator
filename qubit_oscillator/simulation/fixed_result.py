from ..settings import N, DEBUG, DEBUG_PLOT
from qubit_oscillator.constants import (STABILIZERS_Q, HADAMARD, NUM_Q, STAB_SQRT,
                                        PSI0, DELTA)
import numpy as np
import qutip as qt
import warnings
import time
import qubit_oscillator.feedback as fb
import qubit_oscillator.simulation.shared as shared
from qubit_oscillator.simulation.shared import sim_CONTROLLED_FUNC_SWITCH

import qubit_oscillator.plugins.breeding as breeding


def _feedback(state, encoding, exp_info, iterative):
    if encoding.mode == 'state_estimate':
        return -np.angle(qt.expect(STABILIZERS_Q[exp_info.measured_op], state)) + np.pi / 2
    elif encoding.mode in ('cat_breeding_B', 'cat_breeding_BS', 'cat_breeding_C'):
        return breeding.feedback_breeding(encoding, exp_info)
    else:
        return fb.feedback(encoding, iterative=iterative)


def _protocol(state, encoding, exp_info,
                return_outcome, alternating=False, iterative=False, progress_bar=False):
    """
    Do the a single round of the protocol in arxiv/1506.05033, Fig 5
    """
    if progress_bar:
        start_time = time.time()
    controlled_func = sim_CONTROLLED_FUNC_SWITCH[exp_info.method]

    # It is faster to do these in sequence then to multiply first
    state = shared.product(HADAMARD, state)
    if progress_bar:
        print('generated feedback, time passed {}'.format(
                            round((time.time() - start_time), 4)))
        start_time = time.time()
    state = controlled_func(state, exp_info)
    phase = _feedback(state, encoding, exp_info, iterative)
    state = shared.phasegate(state, phase, exp_info)

    state = shared.product(HADAMARD, state)

    if return_outcome or iterative:
        state = state.unit()
        P = qt.expect(NUM_Q, state)
        if exp_info.method == 'me':
            assert P.imag < 0.01, 'P has imaginary leftovers! P: {}'.format(P)
        else:
            assert P.imag < 0.0000001, 'P has imaginary leftovers! P: {}'.format(P)
        P = P.real
    else:
        P = 1 / 2
    P_res = [1 - P, P]
    P_error = [1, 0]
    x = 0
    if iterative:
        true_result = np.random.choice([0, 1], 1, p=P_res)[-1]
        if exp_info._x_m:
            P_error = [1 - exp_info.error_rate, exp_info.error_rate]
            x = np.random.choice([0, 1], 1, p=P_error)[-1]
            exp_info.update_x(x)
        measured_result = true_result ^ x
        encoding = encoding.update_n(measured_result)
    else:
        measured_result = encoding.n_m[encoding.m - 1]
        if exp_info._x_m:
            P_error = [1 - exp_info.error_rate, exp_info.error_rate]
            x = exp_info.x_m[encoding.m - 1]
        true_result = measured_result ^ x
    P = P_res[true_result] * P_error[x]
    if progress_bar:
        print('rotated, passed: {}'.format(round((time.time() - start_time), 4)))
        start_time = time.time()
    state = shared.qubit_reset(state, true_result, exp_info)
    if progress_bar:
        print('Reset, passed: {}'.format(round((time.time() - start_time), 4)))
    if alternating:
        state = shared.product(STAB_SQRT[exp_info.measured_op][encoding.m % 2], state)
    if iterative:
        return state, P, encoding, exp_info
    if return_outcome:
        return state, P
    else:
        return state
    raise Exception('Should never reach this point !')


# The evolution is done according to Fig 10 in arxiv/1506.05033v2 (our paper)
def final_state(encoding, exp_info,
                state0=None, delta=None, track_frame=False, track_rates=False,
                fit_delta=False, sharpness=False, iterative=False, measured_op_type='S',
                progress_bar=False, M=None, track_states=False, return_ev=False,
                weighing=True):
    """
    Return the state after a series of measurements, specified by encoding and exp_info
    """
    if not type(encoding) == fb.Encoding:
        assert len(encoding) == 2
        alternating = True
    else:
        encoding = [encoding]
        exp_info = [exp_info]
        alternating = False

    if state0 is None:
        if delta is None:
            delta = DELTA
            state0 = PSI0
        else:
            # compute the squeezing factor
            r = np.log(1 / delta)
            state0 = qt.tensor(qt.squeeze(N, r) * qt.basis(N, 0), qt.basis(2, 0)).unit()
    state = state0

    if DEBUG and DEBUG_PLOT > 1:
        shared.DEBUG_plot(state, str([e.n_m for e in encoding]) + 'state0')
    P = 1
    rate_list_z = []
    rate_list_x = []
    weight_list = []
    state_list = []

    return_outcome = True
    #if exp_info[0].method in ('weighed circuit', 'weighed evolution'):
    #    return_outcome = True
    #else:
    #    return_outcome = False

    if encoding[0].mode in ['cat_breeding_B', 'cat_breeding_BS', 'cat_breeding_C']:
        breeding.feedback_breeding.reset()
        np.random.seed()
        weight_override = breeding.feedback_breeding(encoding[0], exp_info[0])

    if measured_op_type == 'S':
        ana_ops = ['S_p', 'S_q']
    elif measured_op_type == 'M':
        ana_ops = ['M_p', 'M_q']

    if track_states:
        copy = shared.remove_qubit(state)
        if track_states == 'correct':
            for i in range(alternating + 1):
                copy = shared.correcting_shift_ev(copy, exp_info[i])
        state_list.append(copy)
    elif track_rates:
        copy = shared.remove_qubit(state)
        rate_list_z.append(shared.eigenvalue_std(copy, measured_op=ana_ops[0]))
        rate_list_x.append(shared.eigenvalue_std(copy, measured_op=ana_ops[1]))
        weight_list.append(P)
    if not iterative:
        M = encoding[0].m
    for m in range(1, M + 1):
        for i in range(alternating + 1):
            # NOTE: For m odd, the state is shifted by pi
            if iterative:
                encoding_ = encoding[i]
            else:
                encoding_ = encoding[i].set_m(m)

            if progress_bar:
                start_time = time.time()
                print('starting round {}, alternation {}'.format(m, i))
            state_tuple = _protocol(state, encoding_, exp_info[i],
                                return_outcome=return_outcome, alternating=alternating,
                            iterative=iterative, progress_bar=progress_bar)

            if progress_bar:
                print('analyzing round {}, alternation {}'.format(m, i))

            if iterative:
                state, P_current, encoding[i], exp_info[i] = state_tuple
                P *= P_current
            elif return_outcome:
                state, P_current = state_tuple
                P *= P_current
            else:
                state = state_tuple

            if not weighing:
                warnings.warn('Weighing is off!')
                P = 1

            if DEBUG and DEBUG_PLOT > 1:
                shared.DEBUG_plot(state,
                                  str([e_.n_m for e_ in encoding]) + ' round{}, {}'.format(m, i))

            if track_states:
                copy = shared.remove_qubit(state)
                if return_ev and m == M:
                    ev = shared.eigenvalue_std(copy, exp_info[0].measured_op,
                                                         output='mean') / np.sqrt(2)
                if track_states == 'correct':
                    for i in range(alternating + 1):
                        copy = shared.correcting_shift_ev(copy, exp_info[i])
                state_list.append(copy)
            elif track_rates:
                copy = shared.remove_qubit(state)
                rate_list_z.append(shared.eigenvalue_std(copy, measured_op=ana_ops[0]))
                rate_list_x.append(shared.eigenvalue_std(copy, measured_op=ana_ops[1]))
                weight_list.append(P)
                if progress_bar:
                    print('relative weight {}, rate z {}, rate x {}'.format(
                                      P * 2**m, rate_list_z[-1], rate_list_x[-1]))
            if progress_bar:
                print('round {} alternation {} done, passed: {}'.format(
                                          m, i, round(time.time() - start_time, 4)))

    if encoding[0].mode in ['cat_breeding_B', 'cat_breeding_BS', 'cat_breeding_C']:
        weight_list = np.ones_like(rate_list_z) * weight_override
    if not weighing:
        P = 1
    if track_rates:
        return np.array(rate_list_z), np.array(rate_list_x), np.array(weight_list)
    elif track_states:
        if return_ev:
            return state_list, ev
        else:
            return state_list
    else:
        state = shared.remove_qubit(state)
        if track_frame:
            alpha = 0
            for i in range(alternating + 1):
                alpha = alpha + shared.correcting_shift(state, encoding[i],
                                                        exp_info[i], track_frame=True)
            return state, P, alpha
        else:
            for i in range(alternating + 1):
                state = shared.correcting_shift_ev(state, exp_info[i])
            return state, P
            #if exp_info[0].method in ('weighed circuit', 'weighed evolution'):
            #    return state, P
            #else:
            #    return state
    raise Exception('Should never reach this point !')
