from ..constants import (DEBUG, DEBUG_PLOT_PATH, N, STAB_CONTR,
                         STAB_ROT, H_IDEAL, H_IDLE, CHI, TIDYUP, SX, PROJECT,
                         FEEDBACK_CHOICES, QEYE_N, DISPLACEMENT_VALUES, STABILIZERS)

import qubit_oscillator.wigner3d as w3d

import numpy as np
import scipy
import time
import qutip as qt
import warnings

import qubit_oscillator.feedback as fb
import qubit_oscillator.misc.statistics as stat
import qubit_oscillator.misc.parallel as parallel


class Exp_Info:
    """
    Store all information needed for the simulation

    This includes
    - Simulation method (circuit, master equation or Monte Carlo)
    - Error information (measurement errors, error operators/hamiltonians, error rates, error type)
    - Squeezing of the initial state
    - which operator should be measured (GKP Stabilizer, GKP Pauli, Sensor Stabilizer)

    Class is designed such that invalid arguments (e.g. x_m defined but not
    error_rate) will raise an error
    """
    def __init__(self, x_m=None, method=None, H_error_c=None, H_error_s=None,
                 noise_ops=None, error_rate=None, measured_op="S_p", t_measure=150,
                 mixed_projection=False, imperfect_projection=False,
                 imperfect_rotation=False, delta=None):
        if method == 'weighed circuit':
            method = 'circuit'
            warnings.warn('method for solver is deprecated')
        if method in ('weighed evolution', 'time_evolution'):
            method = 'me'
            warnings.warn('method for solver is deprecated')

        assert method in ('circuit', 'me', 'mc')

        self._error_rate = error_rate
        if error_rate is not None:
            self.error_rate = self._error_rate

        self._delta = delta
        if self._delta is not None:
            self.delta = self._delta

        if x_m is None:
            self._x_m = False
        else:
            self._x_m = True
            self.x_m = x_m

        self.mixed_projection = mixed_projection
        self.imperfect_projection = imperfect_projection
        self.imperfect_rotation = imperfect_rotation
        self.method = method

        # coupled systems
        if H_error_c is None:
            self._H_error_c = False
        else:
            self._H_error_c = True
            self.H_error_c = H_error_c
        # Separated systems (g=0)
        if H_error_s is None:
            self._H_error_s = False
        else:
            self._H_error_s = True
            self.H_error_s = H_error_s

        if noise_ops is None:
            self._noise_ops = False
            self.noise_ops = []
        else:
            self._noise_ops = True
            self.noise_ops = noise_ops

        self.measured_op = measured_op
        self.t_measure = t_measure

        self.legend = ('error_rate', 'x_m', 'mixed_projection', 'imperfect_projection',
                       'imperfect_rotation', 'method', 'H_error_c', 'H_error_s',
                       'noise_ops', 'measured_op', 't_measure')

    def __str__(self):
        settings = (self._error_rate, self._x_m, self.mixed_projection,
                    self.imperfect_projection, self.imperfect_rotation, self.method,
                    self._H_error_c, self._H_error_s, self._noise_ops, self.measured_op,
                    self.t_measure)
        string = ''
        for i in range(len(settings)):
            string += self.legend[i] + ': ' + str(settings[i]) + '\n'
        return string

    def update_x(self, x=None):
        self.x_m.append(x)


def product(oper, state, debug=False):
    """Apply the oper to the state, taking into account whether we have a ket or matrix rep"""
    if state.type == "ket":
        state = oper * state
    elif state.type == "oper":
        state = oper * state * oper.dag()
        state._isherm = None
    return state


# ATTENTION: VERY SLOW
# If possible, keep track of frame for analysis, do not correct explicitely
def displace_(alpha):                               # Displacement operator
    return qt.tensor(qt.displace(N, alpha), qt.qeye(2))


# ATTENTION: VERY SLOW
def phasegate_(phi):                                # Phasegate
    raise Exception('Deprecated')

_PHASE_ERR = np.linspace(-np.pi, np.pi, 2**10 + 1)
_IDX = np.arange(2**10 + 1)


def phasegate(state, phase, exp_info):
    """Apply a gate that rotates the qubit by phase, possibly with noisy rotation"""
    if exp_info.imperfect_rotation is False:
        pg = qt.phasegate(phase)
        pg = qt.tensor(QEYE_N, pg)
        return product(pg, state)
    else:
        sigma = exp_info.error_rate * 2 * np.pi
        if sigma == 0:
            # If the error rate is 0, we can use the ideal phase gate, as above
            pg = qt.phasegate(phase)
            pg = qt.tensor(qt.qeye(N), pg)
            return product(pg, state)
        if state.type in ('ket', 'bra'):
            state = qt.ket2dm(state)
        elif state.type == 'oper':
            pass
        else:
            raise Exception("Type of QuObj not recognized!")
        p = stat.gauss(_PHASE_ERR, sigma)
        idx = np.random.choice(_IDX, size=FEEDBACK_CHOICES, p=p)
        weights = p[idx]
        weights /= np.sum(weights)
        phase_err = _PHASE_ERR[idx]
        for i in range(FEEDBACK_CHOICES):
            w = weights[i]
            pe = phase_err[i]
            pg = qt.phasegate(phase + pe)
            pg = qt.tensor(QEYE_N, pg)
            state_pg = product(pg, state, True)
            if i == 0:
                result = w * state_pg
            else:
                result += w * state_pg
        return result


def circuit_controlled(state, exp_info):
    return product(STAB_CONTR[exp_info.measured_op], state)


def evol_controlled(state, exp_info):
    if exp_info._H_error_c:
        hamiltonian = H_IDEAL + exp_info.H_error_c
    else:
        hamiltonian = H_IDEAL
    state = qt.mesolve(hamiltonian, state, [0, np.pi / (2 * CHI)], exp_info.noise_ops, [],
                       options=qt.Odeoptions(nsteps=5000, tidy=TIDYUP)
                       ).states[-1]
    state = product(SX, state)
    state = product(STAB_ROT[exp_info.measured_op], state)
    state = qt.mesolve(hamiltonian, state, [0, np.pi / (2 * CHI)], exp_info.noise_ops, [],
                      options=qt.Odeoptions(nsteps=5000, tidy=TIDYUP)
                       ).states[-1]
    state = product(SX, state)
    if TIDYUP:
        state = state.tidyup(atol=1e-14)
    return state


def mc_controlled(state, exp_info):
    if exp_info._H_error_c:
        hamiltonian = H_IDEAL + exp_info.H_error_c
    else:
        hamiltonian = H_IDEAL
    state = qt.mcsolve(hamiltonian, state, [0, np.pi / (2 * CHI)], exp_info.noise_ops, [],
                       options=qt.Odeoptions(nsteps=5000, tidy=TIDYUP), ntraj=1
                       ).states[-1]
    state = product(SX, state)
    state = product(STAB_ROT[exp_info.measured_op], state)
    state = qt.mcsolve(hamiltonian, state, [0, np.pi / (2 * CHI)], exp_info.noise_ops, [],
                      options=qt.Odeoptions(nsteps=5000, tidy=TIDYUP), ntraj=1
                       ).states[-1]
    state = product(SX, state)
    if TIDYUP:
        state = state.tidyup(atol=1e-14)
    return state


# Optimized
def qubit_reset(state, x, exp_info):
    """
    Project the qubit to state x and then reset it to 0

    Acts on the joint qubit-cavity state 'state'.

    Args:
        state: Qutip state object, joint cavity-qubit state.
        x: Integer 0 or 1, measurement result

    Returns:
        Qutip state
    """
    assert x in (0, 1), "Non- binary value given to projector"
    if exp_info.imperfect_projection:
        projector = (np.sqrt(1 - exp_info.error_rate) * PROJECT[x]
                   + np.sqrt(exp_info.error_rate) * PROJECT[x ^ 1])
        state = product(projector, state)
    elif exp_info.mixed_projection:
        if state.type in ('ket', 'bra'):
            state = qt.ket2dm(state)
        elif state.type == 'oper':
            pass
        else:
            raise Exception("Type of QuObj not recognized!")
        state = ((1 - exp_info.error_rate) * product(PROJECT[x], state)
                + exp_info.error_rate * product(PROJECT[x ^ 1], state))
    else:
        state = product(PROJECT[x], state)

    if exp_info._noise_ops or exp_info._H_error_s:
        if exp_info._H_error_s:
            hamiltonian = exp_info.H_error_s
        else:
            hamiltonian = H_IDLE
        # qubit reset is instantaneous, but measurement takes time t_measure in ns
        state = qt.mesolve(hamiltonian, state, [0, exp_info.t_measure], exp_info.noise_ops, [],
                           options=qt.Odeoptions(nsteps=5000, tidy=TIDYUP)
                           ).states[-1]
    if TIDYUP:
        state = state.tidyup(atol=1e-14)
    if x == 1:
        state = product(SX, state)
    state = state.unit()
    return state


def remove_qubit(state):
    """
    Remove the qubit from the joint system. Assumes qubit_reset() has been run

    Args:
        state: Qutip state object, joint cavity-qubit state

    Returns:
        Qutip state
    """
    if state.type == "ket":
        # Select the part of the inbound state conditioned on the qubit in state 0.
        # Not a proper ptrace!
        idx = np.arange(0, state.data.shape[0], 2)
        copy = qt.Qobj(state.data[idx])
    elif state.type == "oper":
        copy = state.ptrace(0)
    if TIDYUP:
        copy = copy.tidyup(atol=1e-14)
    return copy.unit()


# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Protocol

def DEBUG_plot(state, comment, n_points_plot=1048, qubit_removed=False):
    # lim=20*np.sqrt(n_points_plot/1024)
    # x_vec = np.linspace(-lim, lim, n_points_plot)
    if not qubit_removed:
        state = remove_qubit(state)
    # w3d.plot_contour(state, figname="./DEBUG/"+str(int(time.time()))+' '+comment+".png")
    w3d.plot_wigner(state,
                    figname=DEBUG_PLOT_PATH + str(int(time.time())) + ' ' + comment + ".png")


def _expect_stabilizer(state, measured_op):
    return qt.expect(STABILIZERS[measured_op], state)


def eigenvalue_std(states, measured_op='S_p', output='std'):
    kwargs = {}
    single = False
    if type(states) is qt.Qobj:
        states = [states]
        kwargs['serial'] = True
        kwargs["progress_bar"] = False
        single = True
    p = parallel.parallel_map(_expect_stabilizer, states, task_args=(measured_op,), **kwargs)
    if single:
        p = p[-1]
    if output == 'std':
        #raise Exception('Seems broken. Should it not be abs**2?')
        return np.sqrt(np.log(1 / np.abs(p)**2)) / (np.abs(DISPLACEMENT_VALUES[measured_op]))
    elif output == 'mean':
        mean = np.angle(p) / (np.sqrt(2) * np.abs(DISPLACEMENT_VALUES[measured_op]))
        return mean


# Optimized, Bottleneck: arbitrary displacement
def correcting_shift(state, encoding, exp_info, track_frame=False, qubit_removed=False):
    """
    Correct the state based on the estimate for n_m.

    Args:
        n_m: list of measurement results
        state: state obtained with measurement results n_m
        mode: mode used for feedback
    Returns:
        array of length x_inter
    """
    est = fb.estimate(encoding)
    alpha = est * SCALING[exp_info.measured_op]
    if track_frame:
        return alpha * np.sqrt(2)
    elif qubit_removed:
        displacement_op = qt.displace(N, alpha)
    else:
        displacement_op = displace_(alpha)
    state = product(displacement_op, state)
    return state


# Optimized, Bottleneck: arbitrary displacement
def correcting_shift_ev(state, exp_info):
    if type(exp_info.measured_op) in (np.float64,):
        est = 1j / np.sqrt(2) * eigenvalue_std(state, exp_info.measured_op, output='mean')
    elif exp_info.measured_op[-2:] == '_p':
        est = 1j / np.sqrt(2) * eigenvalue_std(state, exp_info.measured_op, output='mean')
    elif exp_info.measured_op[-2:] == '_q':
        est = -1 * eigenvalue_std(state, exp_info.measured_op, output='mean')
        warnings.warn('There might be a bug in eigenvalue_std. Check if the output is real!')
    else:
        raise Exception('This measured op is not implemented')
    displacement_op = qt.displace(N, est)
    return product(displacement_op, state)


def approx_wavefunc_0(vec, delta_env, delta_peak, s_max=20):
    s = np.arange(-s_max, s_max + 1).reshape((1, -1))
    vec_ = vec.reshape((-1, 1))
    func = np.exp(-2 * np.pi * delta_env**2 * s**2
                  - (vec_ - 2 * s * np.sqrt(np.pi))**2 / (2 * delta_peak**2))
    func = np.sum(func, axis=-1)
    norm = np.sqrt(scipy.integrate.simps(func**2, vec))
    func = func / norm
    return func


def wigner_codestate(q, p, delta_q, delta_p, s_max=20):
    if DEBUG:
        assert delta_p > 0, 'delta_p <= 0 :, {}'.format(delta_p)
        assert delta_q > 0, 'delta_q <= 0 :, {}'.format(delta_q)
    q = q.reshape(-1, 1)
    p = p.reshape(1, -1)
    s = np.arange(-s_max, s_max + 1)
    nr_splits = np.ceil(len(s) / 11)
    s = np.array_split(s, nr_splits)
    w = 0
    for s1 in s:
        for s2 in s:
            w += _wigner_codestate_helper(q, p, delta_q, delta_p, s1, s2)
    norm = scipy.integrate.simps(w, p.reshape((-1,)), axis=1)
    norm = scipy.integrate.simps(norm, q.reshape((-1,)), axis=0)
    if DEBUG:
        assert round(norm.imag, 7) == 0
        assert round(np.max(w.imag), 7) == 0
    return w.real / norm.real


def _wigner_codestate_helper(q, p, delta_q, delta_p, s1, s2):
    q = q.reshape(-1, 1, 1, 1)
    p = p.reshape(1, -1, 1, 1)
    s1 = s1.reshape(1, 1, -1, 1)
    s2 = s2.reshape(1, 1, 1, -1)
    w_p = - 2j * p * np.sqrt(np.pi) * (s1 - s2)
    w_p -= 1 / 2 * delta_p**2 * (s1**2 + s2**2)
    w_p -= delta_q**2 * p**2
    w_q = - (q - np.sqrt(np.pi) * (s1 + s2))**2 / delta_q**2
    np.exp(w_p, out=w_p)
    w_p = np.sum(w_p, axis=(-1, -2))
    np.exp(w_q, out=w_q)
    w_q = np.sum(w_q, axis=(-1, -2))
    w = w_p * w_q / np.pi
    return w


def wigner_squeezed(q, p, delta_q):
    q = q.reshape(-1, 1)
    p = p.reshape(1, -1)
    w_p = np.exp(-p**2 * delta_q**2)
    w_q = np.exp(-q**2 / delta_q**2)
    w = w_p * w_q / np.pi
    norm = scipy.integrate.simps(w, p.reshape((-1,)), axis=1)
    norm = scipy.integrate.simps(norm, q.reshape((-1,)), axis=0)
    return w.real / norm.real


def overlap(w1, w2, x_vec, y_vec):
    w = 2 * np.pi * w1 * w2
    overlap = scipy.integrate.simps(w, y_vec, axis=1)
    overlap = scipy.integrate.simps(overlap, x_vec, axis=0)
    return overlap


def phase_wigner(func, vec):
    if DEBUG:
        assert vec[0] < 0
        assert round(vec[0] + vec[-1], 7) == 0
        assert len(vec) == len(func)

    pad_amount = 2 * len(vec)
    padded_vec = np.pad(vec, pad_amount, 'linear_ramp',
                       end_values=(2.5 * vec[-1], 2.5 * vec[0]))
    padded_func = np.pad(func, pad_amount, 'constant', constant_values=(0, 0))
    original_window = (np.arange(len(vec)) + pad_amount).reshape(-1, 1)
    offset = np.arange(-len(vec), len(vec) + 1).reshape(1, -1)

    shifted_func = padded_func[original_window + offset]
    shifted_func = shifted_func * np.fliplr(shifted_func.conjugate())
    shifted_func = np.fft.fft(shifted_func, axis=-1)
    shifted_func = np.fft.fftshift(shifted_func, axes=-1)

    new_window = (np.arange(len(vec)) + len(vec))
    new_vec = padded_vec[new_window]
    freqs = np.fft.fftfreqs(len(new_vec), new_vec[-1] - new_vec[0])
    freqs = np.fft.fftshift(freqs)

    return freqs, shifted_func

SCALING = {"S_p": 1j / (2 * np.sqrt(2 * np.pi)),
            "S_q": -1 / (2 * np.sqrt(2 * np.pi)),
            "M_p": 1j / (2 * np.sqrt(np.pi)),
            "M_q": -1 / (2 * np.sqrt(np.pi))}

sim_CONTROLLED_FUNC_SWITCH = {'circuit': circuit_controlled,
                           'me': evol_controlled,
                           'mc': mc_controlled}
