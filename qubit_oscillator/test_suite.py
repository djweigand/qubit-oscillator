def pytest_configure(config):
    # Used to override use of cluster
    import sys
    sys._called_from_test = True

import pytest
import numpy as np
#import qubit_oscillator.feedback as fb
import qubit_oscillator.misc.statistics as stat
#import qubit_oscillator.misc.qo_results as qo_results
import sys


def pytest_unconfigure(config):
    # Remove pytest marker before executing the test code
    del sys._called_from_test

# -----------------------------------------------------------------------------
# -----------------------------------------------------------------------------
# Test feedback
#
#
#class TestEncoding:
#    # List of invalid
#    kwarg_list = [{},
#                  {'m': 0},
#                  {'m': 1},
#                  {'n_m': [1]},
#                  {'m': 1, 'n_m': [1]},
#                  {'m': 1, 'mode': 'arpe'},
#                  ]
#
#    @pytest.mark.parametrize('kwargs', kwarg_list)
#    def test_init_args1(self, kwargs):
#        with pytest.raises(AssertionError):
#            fb.Encoding(**kwargs)
#
#    def test_init_args2(self):
#        with pytest.raises(IndexError):
#            fb.Encoding(**{'m': 2, 'n_m': [1], 'mode': 'arpe'})
#
#    def test_error_rate1(self):
#        with pytest.raises(AttributeError):
#            e = fb.Encoding(m=0, mode='arpe')
#            e.error_rate
#
#    def test_error_rate2(self):
#        e = fb.Encoding(m=0, mode='arpe')
#        assert e._error_rate is None
#
#
#class TestErrorrate0:
#    mode_list = ['rpe', 'arpe', 'random']
#    m_range = range(1, 6)
#    function_list = [fb.feedback, fb.estimate, fb.P, fb.P_av]
#
#    def assert_almost_equal(self, a, b):
#        __tracebackhide__ = True
#        assert np.max(np.abs(np.round(a - b, 7))) == 0
#
#    @pytest.mark.parametrize('m', m_range)
#    @pytest.mark.parametrize('function', function_list)
#    @pytest.mark.parametrize('mode', mode_list)
#    def test_mode_e(self, function, mode, m):
#        result_list = qo_results.result_range(0, 2**m, m)
#        for result in result_list:
#            encoding = fb.Encoding(n_m=result, mode=mode, error_rate=0)
#            encoding_e = fb.Encoding(n_m=result, mode=mode + '_e', error_rate=0)
#            self.assert_almost_equal(function(encoding), function(encoding_e))
#
#    mode_list_full = ['rpe', 'rpe_e', 'arpe', 'arpe_e', 'random', 'random_e']
#
#    @pytest.mark.parametrize('m', m_range)
#    @pytest.mark.parametrize('mode', mode_list_full)
#    def test_P_P_av(self, mode, m):
#        result_list = qo_results.result_range(0, 2**m, m)
#        for result in result_list:
#            encoding = fb.Encoding(n_m=result, mode=mode, error_rate=0)
#            self.assert_almost_equal(fb.P(encoding), fb.P_av(encoding))


class TestStatistics:
    def test_weighted_quantile1(self):
        a = range(3)
        w = None
        assert stat.weighted_quantile(a, 0, weights=w) == 0
        assert stat.weighted_quantile(a, 33, weights=w) == 0
        assert stat.weighted_quantile(a, 34, weights=w) == 1
        assert stat.weighted_quantile(a, 66, weights=w) == 1
        assert stat.weighted_quantile(a, 67, weights=w) == 2
        assert stat.weighted_quantile(a, 100, weights=w) == 2

    def test_weighted_quantile2(self):
        a = np.arange(3)
        w = [1, 2, 1]
        assert stat.weighted_quantile(a, 0, weights=w) == 0
        assert stat.weighted_quantile(a, 25, weights=w) == 0
        assert stat.weighted_quantile(a, 26, weights=w) == 1
        assert stat.weighted_quantile(a, 75, weights=w) == 1
        assert stat.weighted_quantile(a, 76, weights=w) == 2
        assert stat.weighted_quantile(a, 100, weights=w) == 2

    def test_weighted_quantile2(self):
        a = [np.arange(3),np.arange(3)]
        w = [1, 2, 1]
        assert stat.weighted_quantile(a, 0, weights=w, axis=0) == np.array([0, 0])