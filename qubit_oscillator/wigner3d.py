# from qubit_oscillator.constants import *

import matplotlib
if __name__ == '__main__':
    matplotlib.use('Agg')

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator, FuncFormatter, NullFormatter
# It is necessary to import Axes3D for 3D plots, even if it is not used directly
from mpl_toolkits.mplot3d import Axes3D
import scipy.integrate
import qutip as qt
import itertools
import qubit_oscillator.misc.parallel as parallel


def wigner_helper(ij, states, x_vec):
    i, j = ij
    w, y_vec = qt.wigner(states[i][j], x_vec, x_vec, method='fft')
    return w, y_vec


def plot_contour(states, figname='wigner3d_contour.png', encoding_mode='S', lim=10,
                 y_labels=None, title=None, labels=True, progress_bar=0.1, scale=None,
                 cross=False):
    def tickfunc(t, _):
        num = int(np.round(t / (tick_scaling[encoding_mode])))
        if t == 0:
            return r"$0$"
        elif num % 1 != 0:
            return ''
        return label_text[encoding_mode].format(num)

    def tickfunc_empty(t, _):
        return ''

    # Set up states as 2D array, get nrows and ncols
    if type(states) == qt.Qobj:
        ncols = nrows = 1
        states = [[states]]
    elif all(type(state) == qt.Qobj for state in states):
        nrows = 1
        ncols = len(states)
        states = [states]
    else:
        nrows = len(states)
        ncols = max(len(s) for s in states)
    # Figure, Wigner setup
    plt.rc('font',
            size=10,
            **{'family': 'serif', 'serif': []}
            )
    plt.rc('text', usetex=True)

    tick_scaling = {"S": np.sqrt(np.pi),
                    "M": np.sqrt(2 * np.pi)}
    label_text = {"S": r"${:d}\sqrt{{\pi}}$",
                  "M": r"${:d}$"}

    plot_scale = lim * tick_scaling[encoding_mode]

    fig, axes = plt.subplots(nrows, ncols, sharex=True, sharey=True, squeeze=False,
                           #subplot_kw=subplot_kw
                           )
    axes = np.array(axes)
    axes = axes.reshape((nrows, ncols))
    x_vec = np.linspace(-30, 30, 2048)

    majorLocator = MultipleLocator(tick_scaling[encoding_mode])
    minorLocator = MultipleLocator(tick_scaling[encoding_mode])
    majorFormatter = FuncFormatter(tickfunc)

    ij = list(itertools.product(range(nrows), range(ncols)))
    w, y_vec = list(zip(*parallel.parallel_map(wigner_helper, ij,
                              task_args=(states, x_vec),
                              local=True, progress_bar=progress_bar)))
    w = np.array([[w[ncols * j + i] for i in range(ncols)] for j in range(nrows)])
    y_vec = y_vec[-1]
    for i, state_row in enumerate(states):
        for j, state in enumerate(state_row):
            #w, y_vec = qt.wigner(state, x_vec, x_vec, method='fft')
            ax = axes[i, j]
            ax.xaxis.set_major_locator(majorLocator)
            ax.xaxis.set_minor_locator(minorLocator)
            ax.yaxis.set_major_locator(majorLocator)
            ax.yaxis.set_minor_locator(minorLocator)
            if labels:
                ax.xaxis.set_major_formatter(majorFormatter)
                ax.yaxis.set_major_formatter(majorFormatter)
            else:
                pass
                ax.xaxis.set_major_formatter(FuncFormatter(tickfunc_empty))
                ax.yaxis.set_major_formatter(FuncFormatter(tickfunc_empty))
            #ax.grid(alpha=0.4, color='black', linewidth=0.5)
            ax.grid(which='minor', alpha=0.4, color='black',
                    linewidth=0.25)
            #ax.minorticks_off()
            ax.tick_params(which='both', direction='in', bottom=True,
                           top=True, left=True, right=True)
            ax.tick_params(which='major', length=6)
            ax.tick_params(which='minor', length=3)
            if cross:
                #ax.axhline(0, linewidth=1.3, color='orange', linestyle='-')
                ax.axhline(0, linewidth=1, color='white', linestyle='-')
                ax.axhline(0, linewidth=0.75, color='black', linestyle='-')
                #ax.axvline(0, linewidth=1.3, color='orange', linestyle='-')
                ax.axvline(0, linewidth=1, color='white', linestyle='-')
                ax.axvline(0, linewidth=0.75, color='black', linestyle='-')
            if labels and y_labels is not None and j == 0:
                ax.set_ylabel(y_labels[i], labelpad=0)
            if True:
                w_ = w[i, j]
                w_ = w_[np.ix_(np.abs(y_vec) < plot_scale,
                               np.abs(x_vec) < plot_scale)]
                ax.pcolorfast((-plot_scale, plot_scale), (-plot_scale, plot_scale), w_,
                            norm=matplotlib.colors.Normalize(-w.max(), w.max()),
                            cmap=plt.get_cmap('seismic_r'),
                            )
    #fig_scale = 3.405 / (max(ncols, nrows))
    fig_scale = 3.405
    if scale == 'paper':
        fig.set_size_inches(2 * fig_scale, 2 * fig_scale / 4)
    elif scale == 'auto':
        fig.set_size_inches(ncols * fig_scale, nrows * fig_scale)
    elif scale == 'talk':
        fig.set_size_inches(0.75 * fig_scale, 0.75 * fig_scale)
    elif scale == 'square':
        fig.set_size_inches(fig_scale/2, fig_scale/2)

    plt.tight_layout(pad=0.5, h_pad=0, w_pad=0)

    if labels:
        big_ax = fig.add_subplot(111, frameon=False)
        big_ax.tick_params(labelcolor='none', top='off', bottom='off', left='off', right='off')
        big_ax.set_xlabel("$q/\sqrt{{2\pi}}$", labelpad=0)
        big_ax.set_ylabel("$p/\sqrt{{2\pi}}$", labelpad=0)
        if title is not None:
            big_ax.set_title(title)

    plt.tight_layout(pad=0.2, h_pad=0, w_pad=0)
    fig.savefig(figname, dpi=600, pad_inches=0, bbox_inches='tight')
    #fig.savefig(figname, pad_inches=0)
    plt.close()


def plot_wigner(state, figname='../figures/wigner3d.png', encoding_mode="S", lim=10):
    if isinstance(state, list) and len(state) == 2:
        state, figname = state
    # needed to supress the outmost ticks, they mess up the grid
    offset = 10**(-6)
    zmin = -(0.5 - offset)
    zmax = 1
    plt.rc('font', size=10,
            **{'family': 'serif', 'serif': []}
            )
    plt.rc('text', usetex=True)
    tick_scaling = {"S": np.sqrt(np.pi),
                    "M": np.sqrt(2 * np.pi)}
    label_text = {"S": r"${:d}\sqrt{{\pi}}$",
                  "M": r"${:d}$"}
    axis_label_text = {"S": [r"$q/\sqrt{{\pi}}$", r"$p/\sqrt{{\pi}}$"],
                  "M": [r"$q/\sqrt{{2\pi}}$", r"$p/\sqrt{{2\pi}}$"]}

    len_scale = lim * tick_scaling[encoding_mode]

    def tickfunc(t, _):
        num = int(np.round(t / (tick_scaling[encoding_mode])))
        if t == 0:
            return r"$0$"
        elif num % 2 != 0:
            return ''
        return label_text[encoding_mode].format(num)

    def tickfunc_z(t, _):
        if t == 0:
            return r"$0$"
        return r"${}$".format(t)

    majorLocator = MultipleLocator(tick_scaling[encoding_mode])
    majorFormatter = FuncFormatter(tickfunc)
    minorLocator = MultipleLocator(tick_scaling[encoding_mode])

    zMajorLocator = MultipleLocator(0.25)
    zMinorLocator = MultipleLocator(0.25)

    #x_vec = np.linspace(-30, 30, 2048)
    #x_vec = np.linspace(-len_scale, len_scale, int(len_scale * 200))
    x_vec = np.linspace(-20, 20, 1024)
    #x_vec = np.linspace(-20, 20, 100)
    w, y_vec = qt.wigner(
                      state,
                      x_vec,
                      x_vec,
                      method='fft')  # state n_m = 0 of biggest M

    cond_x = np.abs(x_vec) < len_scale
    cond_y = np.abs(y_vec) < len_scale

    x_vec = np.compress(cond_x, x_vec)
    y_vec = np.compress(cond_y, y_vec)

    w = np.compress(cond_x, w, axis=1)
    w = np.compress(cond_y, w, axis=0)

    pfunc = scipy.integrate.simps(w, x_vec, axis=1)
    qfunc = scipy.integrate.simps(w, y_vec, axis=0)
    if False:
        print('resolution x', np.diff(x_vec)[-1])
        print('resolution y', np.diff(y_vec)[-1])
        print('Integrated wigner', scipy.integrate.simps(pfunc, y_vec),
          scipy.integrate.simps(qfunc, x_vec))
    # mask out values outside of the boundaries for q- and p-projections
    pfunc[pfunc > zmax] = np.NaN
    qfunc[qfunc > zmax] = np.NaN
    #zmax = np.max([pfunc.max(), qfunc.max()])

    X, Y = np.meshgrid(x_vec, y_vec)

    points = np.stack([X, Y], axis=2)
    points = np.reshape(points, (-1, 2))

    fig2 = plt.figure()
    ax2 = fig2.add_subplot(111, projection='3d')
    fig2.set_size_inches(3.405, 3.405)
    fig2.subplots_adjust(left=0, right=1, bottom=0, top=1)

    ax2.grid(zorder=1)
    #axes3d.labelpad

    ax2.xaxis.pane.set_edgecolor('black')
    ax2.yaxis.pane.set_edgecolor('black')
    ax2.xaxis.pane.fill = False
    ax2.yaxis.pane.fill = False
    ax2.zaxis.pane.fill = False

    xlbl = ax2.set_xlabel(axis_label_text[encoding_mode][0], labelpad=0)
    ax2.set_xlim(-(len_scale - offset), (len_scale - offset))
    ylbl = ax2.set_ylabel(axis_label_text[encoding_mode][1], labelpad=0)
    ax2.set_ylim(-(len_scale - offset), (len_scale - offset))
    ax2.set_zlim(zmin, zmax,)
    ax2.tick_params(axis='x', pad=0)
    ax2.tick_params(axis='y', pad=0)
    ax2.tick_params(axis='z', pad=0)

    ax2.xaxis.set_major_locator(majorLocator)
    ax2.xaxis.set_major_formatter(majorFormatter)
    ax2.xaxis.set_minor_locator(minorLocator)

    ax2.yaxis.set_major_locator(majorLocator)
    ax2.yaxis.set_major_formatter(majorFormatter)
    ax2.yaxis.set_minor_locator(minorLocator)

    ax2.zaxis.set_major_locator(zMajorLocator)
    #ax2.zaxis.set_major_formatter(NullFormatter)
    ax2.zaxis.set_ticklabels([])
    ax2.zaxis.set_minor_locator(zMinorLocator)

    [t.set_va('center') for t in ax2.get_yticklabels()]
    [t.set_ha('left') for t in ax2.get_yticklabels()]
    [t.set_va('center') for t in ax2.get_xticklabels()]
    [t.set_ha('right') for t in ax2.get_xticklabels()]
    [t.set_va('center') for t in ax2.get_zticklabels()]
    [t.set_ha('left') for t in ax2.get_zticklabels()]

    ax2.xaxis._axinfo['tick']['inward_factor'] = 0
    ax2.xaxis._axinfo['tick']['outward_factor'] = 0.4
    ax2.yaxis._axinfo['tick']['inward_factor'] = 0
    ax2.yaxis._axinfo['tick']['outward_factor'] = 0.4
    ax2.zaxis._axinfo['tick']['inward_factor'] = 0
    ax2.zaxis._axinfo['tick']['outward_factor'] = 0.4

    ax2.view_init(elev=25)
    ax2.dist = 9.5

    w_clip = w
    w_clip[np.abs(w_clip) < 10**(-4)] = 0

    ax2.contour(
                X, Y, w_clip, 5,
                zdir='z',
                offset=zmin - 0.03,
                cmap=plt.get_cmap('seismic_r'),
                norm=matplotlib.colors.Normalize(-w_clip.max(), w_clip.max()),
                alpha=0.5,
                antialiased=True,
                #linewidth = 1
                zorder=0,
                #rasterized=True
                )

    ax2.plot(
            y_vec, pfunc,
            zdir='x',
            zs=-len_scale - 0.3,
            zorder=5,
            color='black',
            )

    ax2.plot(
            x_vec, qfunc,
            zdir='y',
            zs=len_scale + 0.3,
            zorder=5,
            color='black',
            )

    ax2.plot_surface(X, Y, w,
                    rstride=1,
                    cstride=1,
                    #facecolors=illuminated_surface,
                    antialiased=True,
                    linewidth=0,
                    #color='black',
                    cmap=plt.get_cmap('seismic_r'),
                    norm=matplotlib.colors.Normalize(-w.max(), w.max()),
                    zorder=10,
                    alpha=0.9,
                    #rasterized = True
                    )
    plt.tight_layout()
    fig2.savefig(figname, dpi=300, bbox_inches='tight', pad_inches=0,
                 bbox_extra_artists=(xlbl, ylbl))
    plt.close(fig2)


def plot_wigner_list(state_list, encoding_mode="S", prefix="wigner3d"):
    arg_list = []
    for i, state in enumerate(state_list):
        arg_list.append([state, '../figures/{}_{}.png'.format(prefix, i)])
    misc.parallel_map_(plot_wigner, arg_list,
                       task_kwargs={"encoding_mode": encoding_mode})
