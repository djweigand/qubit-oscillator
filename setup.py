from setuptools import setup

setup(name='qubit_oscillator',
      version='5.1',
      description='Tools for simulating encoding of a qubit in an oscillator',
      author='Daniel Weigand',
      author_email='weigand@physik.rwth-aachen.de',
      packages=['qubit_oscillator'],
      install_requires=[
          'lxt_cluster'
      ],
      zip_safe=False)
