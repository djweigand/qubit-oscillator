"""
This module wraps many parameter sets.

This is the exectued python script. For each parameter set, kerr.py is launched in a subprocess.
The settings are given to kerr.py in form of arguments, see the gigantic strings constructed at the end of this file.
Documentation of these arguments is found in kerr.py
"""

import subprocess
import time
import datetime
import signal
import ctypes


libc = ctypes.CDLL("libc.so.6")         # ???
def set_pdeathsig(sig=signal.SIGTERM):
    """UNKNOWN PURPOSE"""
    def callable():
        return libc.prctl(1, sig)
    return callable


def is_working_time(start=9, end=18, min_diff=0):
    """
    Test if the current time is within office hours

    Useful for lengthy calculations on other people's machines.
    24h format
    start: begin of office hours
    end: end of office hours
    min_diff: Do not start job if office hours begin in min_diff hours.
        This is useful if the jobs take a long time each, as they will complete once started.
    """
    assert min_diff < start, 'min_diff > start is not implemented'
    now = datetime.datetime.now()
    if now.weekday() in (5,6):
        # Test for Weekend
        return False
    elif now.hour >= end or now.hour+min_diff < start:
        # Test Nighttime
        return False
    else:
        return True

# SETTINGS
scheduler = False                           # If True, jobs will only run outside of office hours
script_name = 'kerr'                        # Name of the python file that will be executed.
# 'photon_loss': [0, 1, 2, 3, 5],
rates_dict = {'photon_loss': [0, 3, 5],
              'photon_loss_alter': [0, 5],
              'amplitude_damping': [0, 10, 20],
              'imperfect_projection': [0, 0.01, 0.025, 0.05, 0.1],
              'measurement_errors': [0, 0.01, 0.025, 0.05, 0.1],
              'kerr_sz': [0, 0.5, 2],
              'kerr_qeye': [0, 0.1, 0.5],
              'imperfect_rotation': [0, 0.1, 0.15, 0.2, 0.25],
              'noiseless': [0]}

#M = 8
#mode_list = [ 'state_estimate', 'estimate']
#error_type_list = ['photon_loss', 'amplitude_damping']
#method_list = ['weighed evolution', 'weighed evolution']
#overwrite_list = [False, False]
#nr_result_list = [0, 0]
#alternating_list = [False, False]
#delta_list = [0.2, 0.2]

#M = 12
#mode_list = [ 'state_estimate', 'estimate']
#error_type_list = ['kerr_sz', 'kerr_qeye', 'kerr_sz', 'kerr_qeye', 'measurement_errors', 'imperfect_projection']
#method_list = ['weighed evolution', 'weighed evolution', 'weighed evolution', 'weighed evolution', 'weighed circuit', 'weighed circuit']
#nr_result_list = [2000, 2000, 0, 0, 0, 0]
#delta_list = [1, 1, 0.2, 0.2, 0.2, 0.2]
#alternating_list = [True, True, False, False, False, False]
#overwrite_list = [False, False, False, False, False, False]

if False:
    # Cat breeding: imperfect rotation
    M = 12
    mode_list = ['cat_breeding_C']
    error_type_list = ['imperfect_rotation']
    method_list = ['circuit']
    nr_result_list = [0]
    delta_list = [0.2]
    alternating_list = [False]
    overwrite_list = [False]
    plot_flags = 'cat_breeding'
    measured_op_type = 'M'
elif True:
    # Cat breeding: noiseless
    M = 12
    #mode_list = ['fully_random', 'cat_breeding_B', 'off', 'estimate', 'arpe', 'state_estimate']
    mode_list = ['fully_random']
    #mode_list = ['cat_breeding_B', 'off', 'estimate']
    error_type_list = ['noiseless']
    method_list = ['circuit']
    nr_result_list = [0]
    delta_list = [0.2]
    alternating_list = [False]
    overwrite_list = [True]
    plot_flags = 'cat_breeding'
    measured_op_type = 'M'
elif False:
    # aps talk: noiseless
    M = 12
    mode_list = ['state_estimate']
    error_type_list = ['noiseless']
    method_list = ['circuit']
    nr_result_list = [0]
    delta_list = [0.2]
    alternating_list = [False]
    overwrite_list = [False]
    plot_flags = 'aps-noiseless'
    measured_op_type = 'M'
elif False:
    # aps talk: kerr
    M = 12
    mode_list = ['state_estimate']
    error_type_list = ['kerr_sz', 'kerr_qeye', 'measurement_errors']
    method_list = ['me', 'me', 'circuit']
    nr_result_list = [0, 0, 0]
    delta_list = [0.2, 0.2, 0.2]
    alternating_list = [False, False, False]
    overwrite_list = [False, False, False]
    plot_flags = 'aps-noise'
    measured_op_type = 'M'
elif False:
    # aps talk: photon loss, amplitude damping
    M = 8
    mode_list = ['state_estimate']
    error_type_list = ['amplitude_damping', 'photon_loss']
    method_list = ['me', 'me']
    nr_result_list = [0, 0]
    delta_list = [0.2, 0.2]
    alternating_list = [False, False]
    overwrite_list = [False, False]
    plot_flags = 'aps-noise'
    measured_op_type = 'M'
elif False:
    # aps talk: altern photon loss
    M = 8
    mode_list = ['state_estimate']
    error_type_list = ['photon_loss_alter']
    method_list = ['me']
    nr_result_list = [200]
    delta_list = [1]
    alternating_list = [True]
    overwrite_list = [False]
    plot_flags = 'aps-noise'
    measured_op_type = 'M'
elif False:
    M = 12
    #mode_list = ['cat_breeding_B', 'cat_breeding_BS', 'cat_breeding_C', 'estimate']
    #mode_list = ['cat_breeding_B', 'cat_breeding_C', 'state_estimate']
    mode_list = ['state_estimate', 'arpe', 'rpe']
    mode_list = ['cat_breeding_C']
    #error_type_list = ['cat_breeding']
    error_type_list = ['noiseless']
    #method_list = ['weighed circuit']
    method_list = ['circuit']
    nr_result_list = [0]
    delta_list = [0.2]
    alternating_list = [False]
    overwrite_list = [False]

script_list = []
for error_type, method, nr_results, alternating, delta, overwrite in zip(error_type_list, method_list,
                        nr_result_list, alternating_list, delta_list, overwrite_list):
    rates = rates_dict[error_type]
    if error_type == 'photon_loss_alter':
        alter = True
        error_type = 'photon_loss'
    else:
        alter = False
    data_file = error_type
    if False or overwrite:
        for rate in rates:
            for mode in mode_list:
                script_list.append(['-rate_list {}'.format(rate), '-mode_list {}'.format(mode),
                                    '-delta {}'.format(delta), '-M {}'.format(M),
                                    '-error_type {}'.format(error_type),
                                    '-nr_results {}'.format(nr_results), '-alternating {}'.format(alternating),
                                    '-overwrite {}'.format(overwrite),
                                    '-plot False', '-print_keys True', "-method '{}'".format(method),
                                    '-measured_op_type {}'.format(measured_op_type), '-data_file {}'.format(data_file),
                                    '-plot_flags {}'.format(plot_flags)])

    rate_list_plot = [str(rate) for rate in rates]
    rate_list_plot = " ".join(rate_list_plot)

    if alter:
        figname_plot = error_type + '_alter'
    else:
        figname_plot = error_type + '_all'
    mode_list_plot = " ".join(mode_list)
    plot_mode_plot = " ".join(mode_list)
    script_list.append(['-rate_list {}'.format(rate_list_plot), '-mode_list {}'.format(mode_list_plot),
                    '-plot_mode {}'.format(plot_mode_plot),
                            '-delta {}'.format(delta), '-M {}'.format(M),
                            '-error_type {}'.format(error_type),
                            '-nr_results {}'.format(nr_results),  '-alternating {}'.format(alternating),
                            '-overwrite False', '-heralding_cut 1',
                            '-plot True', '-print_keys True', '-figname {}'.format(figname_plot),
                            "-method '{}'".format(method), '-measured_op_type {}'.format(measured_op_type),
                            '-scale_sigma sqrt_2','-plot_flags {}'.format(plot_flags),
                            '-data_file {}'.format(data_file)])

print("Running {} scripts:".format(len(script_list)))
#for s in script_list:
    #print(s)
for args in script_list:
    while True:
        if (not scheduler) or (not is_working_time(8, 18, 3)):
            break
        print("Waiting until evening/weekend, time {}".format(datetime.datetime.now()), end='\r')
        time.sleep(600)
    #call_args = 'chrt --idle 0 mpirun -n 3 python3 -u '+script_name+'.py '
    call_args = 'chrt --idle 0 python3 -u '+script_name+'.py '
    for arg in args:
        call_args += ' '+arg
    print('\n Wrapper Running:')
    print(datetime.datetime.now())
    print(call_args)
    try:
        subprocess.call(call_args, shell=True, preexec_fn=set_pdeathsig())
    except Exception as e:
        print('\n ERROR ERROR ERROR')
        print(e)
        print('\n')
    else:
        print('\n Wrapper Round Done \n')
    time.sleep(5)
